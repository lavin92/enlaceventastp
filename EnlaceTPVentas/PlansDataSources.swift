//
//  PlansDataSources.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 26/09/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import Foundation
import UIKit

class PlansDataSources: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var tableView: UITableView?
    var plansArray : [Plans] = []
    
    init(tableView:UITableView) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource=self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 250
        self.tableView?.rowHeight = UITableViewAutomaticDimension
    }
    
    func update(coleccionInfo: [Plans]){
        self.plansArray = coleccionInfo
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return plansArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "CellDetailTableViewCell"
        
        let cell:CellDetailTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier, for:indexPath)as! CellDetailTableViewCell
        
        let addOn = self.plansArray[indexPath.row]
        
        cell.tittleCellLabel.text = addOn.tittle
        cell.tittle2CellLabel.text = addOn.tittle2
        cell.tittle3CellLabel.text = addOn.tittle3
        cell.imageCellImageView.image  = UIImage(named:addOn.image!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 250;//Choose your custom row height
    }
    
}
