//
//  Cot_Sitio.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 02/03/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class Cot_Sitio: NSObject, Mappable {

    var cotSitio: [String] = []
    
    required convenience init?(map: Map) {
        self.init()
       
    }
    
    public func mapping(map: Map) {
        
        cotSitio <- map["Cot_Sitio"]
    }
    
}
