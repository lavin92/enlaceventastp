//
//  ConsultaPlanesPresenter.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 22/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

public protocol ConsultaPlanesDelegate : NSObjectProtocol {
    func onSuccessConsultaPlanes()
}


class ConsultaPlanesPresenter: BaseVentasPresenter {
    
    var mConsultaPlanesDelegate : ConsultaPlanesDelegate!
    
    init(viewController: BaseViewController, consultaPlanesDelegate : ConsultaPlanesDelegate) {
        super.init(viewController: viewController)
        mConsultaPlanesDelegate = consultaPlanesDelegate
    }
    

    func consultaPlanes(typeRegistrationCotation : String, typeMoney : String, city : String, servicePlanType: String, typeCoverage : String, subtypeOpportunity : String){
        let consultaPlanesRequest : ConsultaPlanesRequest = ConsultaPlanesRequest()
        consultaPlanesRequest.typeRegistrationCotation = typeRegistrationCotation
        consultaPlanesRequest.typeMoney = typeMoney
        consultaPlanesRequest.city = city
        consultaPlanesRequest.servicePlanType = servicePlanType
        consultaPlanesRequest.typeCoverage = typeCoverage
        consultaPlanesRequest.subtypeOpportunity = subtypeOpportunity
        RetrofitManager<ConsultaPlanesResponse>.init(requestUrl: ApiDefinition.WS_CONSULTA_PLANES, delegate: self).request(requestModel: consultaPlanesRequest)
        
    }
    
    func onRequestWs(requestUrl : String, messageError: String) {
        if requestUrl == ApiDefinition.WS_CONSULTA_PLANES{
            AlertDialog.hideOverlay()
        }
    }
    
   
    
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
    AlertDialog.hideOverlay()
        if requestUrl == ApiDefinition.WS_CONSULTA_PLANES{
        let consultaPlanesResponse :ConsultaPlanesResponse = response as! ConsultaPlanesResponse
        if consultaPlanesResponse.result?.rResult == "0"{
        var arrayDetallePlanes : [ArrayDetallePlanes] = consultaPlanesResponse.arrayDetallePlanes
                
            }
                
        
        }
        
        
    
    }

}
