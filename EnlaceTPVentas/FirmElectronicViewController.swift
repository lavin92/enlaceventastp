//
//  FirmElectronicViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 12/01/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class FirmElectronicViewController: BaseViewController {

    @IBOutlet weak var restoreButton: UIButton!
    @IBOutlet weak var firmDrawView: YPDrawSignatureView!
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let mColor = UIColor.black
        let myColor = UIColor.red
        restoreButton.layer.borderColor = myColor.cgColor
        saveButton.layer.borderColor = myColor.cgColor
        firmDrawView.layer.borderColor = mColor.cgColor
        
        restoreButton.layer.borderWidth = 1.0
        saveButton.layer.borderWidth = 1.0
        firmDrawView.layer.borderWidth = 1.0

    }
    
    @IBAction func restoreButton(_ sender: Any) {
        self.firmDrawView.clear()
        
    }
    
    @IBAction func saveButton(_ sender: Any) {
        if let signatureImage = self.firmDrawView.getSignature(scale: 10) {
            
            // Saving signatureImage from the line above to the Photo Roll.
            // The first time you do this, the app asks for access to your pictures.
            //UIImageWriteToSavedPhotosAlbum(signatureImage, nil, nil, nil)
            
            // Since the Signature is now saved to the Photo Roll, the View can be cleared anyway.
            self.firmDrawView.clear()
            resultValue = .RESULT_OK
            data ["Siganture"] = signatureImage as AnyObject?
            self.navigationController?.popViewController(animated: true)
            
            dismiss(animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
