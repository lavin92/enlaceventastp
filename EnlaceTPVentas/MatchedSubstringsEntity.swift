//
//  MatchedSubstringsEntity.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 19/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper


class MatchedSubstringsEntity: NSObject,Mappable {

    var lenght : String?
    var offset : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    public func mapping(map: Map) {
     
        
        lenght <- map["lenght"]
        offset <- map["offset"]
    }

}
