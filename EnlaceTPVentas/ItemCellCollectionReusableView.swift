//
//  ItemCellCollectionReusableView.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 25/09/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit

class ItemCellCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var imageCellImageView: UIImageView!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}

