//
//  PaymentMethod.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 29/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class PaymentMethod: NSObject, Mappable {
    
    var method : String = ""
    var cardType : String = ""
    var nameTitular : String = ""
    var paternalLastNameTittle : String = ""
    var mothersLastNameTittle : String = ""
    var cardNumber : String = ""
    var expirationMonth : String = ""
    var expirationYear: String = ""
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    
    public func mapping(map: Map) {
        
        method <- map ["Metodo"]
        cardType <- map ["TipoTarjeta"]
        nameTitular <- map ["NombreTitular"]
        paternalLastNameTittle <- map ["ApellidoPaternoTitular"]
        mothersLastNameTittle <- map ["ApellidoMaternoTitular"]
        cardNumber <- map ["NumeroTarjeta"]
        expirationMonth <- map ["VencimientoMes"]
        expirationYear <- map ["VencimientoAnio"]
    }
}

