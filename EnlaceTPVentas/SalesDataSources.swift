//
//  SalesDataSources.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 02/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import Foundation
import UIKit

class SalesDataSources: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var tableView: UITableView?
    var salesArray : [Sales] = []
    
    init(tableView:UITableView) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource=self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 143
        self.tableView?.rowHeight = UITableViewAutomaticDimension
    }
    
    func update(coleccionInfo: [Sales]){
        self.salesArray = coleccionInfo
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return salesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "CellSalesStatusTableViewCell"
        
        let cell:CellSalesStatusTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier, for:indexPath)as! CellSalesStatusTableViewCell
        
        let addOn = self.salesArray[indexPath.row]
        
        cell.tittleCellLabel.text = addOn.tittle
        cell.tittle2CellLabel.text = addOn.tittle2
        cell.tittle3CellLabel.text = addOn.tittle3
        cell.tittle4CellLabel.text = addOn.tittle4
        cell.tittle5CellLabel.text = addOn.tittle5
        cell.tittle6CellLabel.text = addOn.tittle6
        cell.tittle7CellLabel.text = addOn.tittle7
        cell.imageCellImageView.image  = UIImage(named:addOn.image!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 143;//Choose your custom row height
    }
    
}

