//
//  Definitions.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 15/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit

class ApiDefinition: NSObject {
    
    
    //API MAPS
    
    
    static let GEO_CODING_BASE_URL = "https://maps.googleapis.com/maps/api/geocode/json?"
    static let GEO_CODING_API_KEY = "AIzaSyBvPtCfXHLfAtgHmfdNJBOiRLMVnkQdAOQ"
    
    //    static let API_TOTAL = "https://msstest.totalplay.com.mx"
    static let API_TOTAL = "https://189.203.181.233:443"
    
    static let WS_LOGIN = API_TOTAL + "/ventasmovilEmpresarial/LoginSF"
    static let WS_CONSULTA_PLANES = API_TOTAL + "/ventasmovilEmpresarial/ConsultaPlanes"
    static let WS_COBERTURA = API_TOTAL + "/ventasmovilEmpresarial/Cobertura "
    static let WS_CONSULTA_STATUS_SOLICITUD = API_TOTAL + "/ventasmovilEmpresarial/ConsultaStatusSolicitud "
    static let WS_DETALLE_SOLICITUD = API_TOTAL + "/ventasmovilEmpresarial/DetalleSolicitud "
    static let WS_DETALLE_PLAN = API_TOTAL + "/ventasmovilEmpresarial/DetallePlan "
    static let API_CP = API_TOTAL + "/ventasmovil/ConsultaCP"
    static let WS_COVERAGE_VALIDATE = API_TOTAL + "/ventasmovilEmpresarial/Cobertura"
}

