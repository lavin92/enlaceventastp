//
//  Info.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 31/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class Info: NSObject , Mappable {

    var iIdPlan : String?
    
    
    
    required convenience init?(map: Map){
        self.init()
        
    }
    
    override init() {
        
    }
    
     init(iIdPlan : String) {
        self.iIdPlan = iIdPlan
    }
    
    public func mapping(map: Map) {
        
      iIdPlan <- map["IdPlan"]
      
    }
}
