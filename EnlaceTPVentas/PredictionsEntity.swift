//
//  PredictionsEntity.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 19/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class PredictionsEntity: NSObject,Mappable {

    var descripcion: String?
    var id : String?
    var placeId : String?
    var reference : String?
    var matchedSubstringsEntity : [MatchedSubstringsEntity] = []
    var terms : [Terms] = []
    
    
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    public func mapping(map: Map) {
        
    }
    
}
