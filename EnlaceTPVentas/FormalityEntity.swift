//
//  FormalityEntity.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 16/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class FormalityEntity: NSObject, Mappable{
    
    var login: Login?
    var prospectus: Prospectus?
    var accountId: String?
    var oportunityId: String?
    var siteId: String?
    var quotationId: String?
    var accountBill: String?
    var accountBRM: String?
    /////
    var totalPriceSoonPayment: Double?
    var totalPriceList: Double?
    var tatalPriceBase: Double?
    var sumPrecioBase: Double?
    var totalProntoPago: Double?
    var sumAddons: Double?
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    public func mapping(map: Map) {
        
      login <- map["Login"]
      prospectus <- map["Prospecto"]
        
    }

}
