//
//  LoginPresenter.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 22/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

public protocol LoginDelegate : NSObjectProtocol {
    func onSuccessLogin(nameEmployee : String)
}

class LoginPresenter: BaseVentasPresenter {
    
    var mLoginDelegate : LoginDelegate!
    
    var mUserEmployee: String?
    var mPasswordEmployee: String?
    
    init(viewController: BaseViewController, loginDelegate : LoginDelegate) {
        super.init(viewController: viewController)
        mLoginDelegate = loginDelegate
    }
    
    func login(username : String, password : String){
        let loginRequest : LoginRequest = LoginRequest()
        loginRequest.nEmployee = username
        loginRequest.password = password
        
        mUserEmployee = username
        mPasswordEmployee = password
        
        RetrofitManager<LoginResponse>.init(requestUrl: ApiDefinition.WS_LOGIN, delegate: self).request(requestModel: loginRequest)
    }
    
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        AlertDialog.hideOverlay()
        if requestUrl == ApiDefinition.WS_LOGIN {
            let loginResponse : LoginResponse = response as! LoginResponse
            if loginResponse.response?.rResult == "0"  {
                
              UserDefaults.standard.set(mUserEmployee, forKey: "username")
              UserDefaults.standard.set(mPasswordEmployee, forKey: "password")
                
                var infoUser : InfoUser = loginResponse.infoUser!
                mDataManager.tx(execute: { (tx) -> Void in
                    tx.save(object: infoUser)
                })
                mLoginDelegate.onSuccessLogin(nameEmployee: infoUser.iName!)
            } else {
                if(loginResponse.response?.rResult == nil ){
                    onErrorLoadResponse(requestUrl: requestUrl, messageError: "Error al contactar al servidor")
                }else{
                    onErrorLoadResponse(requestUrl: requestUrl, messageError: (loginResponse.response?.rDescription)!)
                }
                
            }
        }
    }
    
    
    
}

