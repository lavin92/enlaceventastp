//
//  ListDataSources.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 24/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit

class ListDataSources: NSObject,UITableViewDataSource, UITableViewDelegate {
    
    var tableView: UITableView?
    var listArray : [List] = []
    
    init(tableView:UITableView) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource=self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 50
        self.tableView?.rowHeight = UITableViewAutomaticDimension
    }
    
    func update(coleccionInfo: [List]){
        self.listArray = coleccionInfo
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "CellAdditionalSettingsTableViewCell"
        
        let cell:CellAdditionalSettingsTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier, for:indexPath)as! CellAdditionalSettingsTableViewCell
        
        let addOn = self.listArray[indexPath.row]
        
        cell.tittleCellLabel.text = addOn.tittle
        
        //cell.imageCellImageView.image  = UIImage(named:addOn.image!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50;//Choose your custom row height
    }
    
}


