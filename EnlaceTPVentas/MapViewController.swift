//
//  MapViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 25/09/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftBaseLibrary
import CoreLocation


class MapViewController: BaseViewController, PredictionAddressDelegate, TableViewCellClickDelegate, GeocoderDelegate,MapLocationSearchDelegate,CLLocationManagerDelegate,GMSMapViewDelegate{
    @IBOutlet weak var mListAddressTableView: UITableView!
    @IBOutlet weak var mGoogleMapView: GMSMapView!
    
    @IBOutlet weak var mSearchTextField: UITextField!
    @IBOutlet weak var nextContinueButton: UIButton!
    @IBOutlet weak var searchView: UIView!
    var autoCompleteDirection: [String] = []
    var autoComplete: [String] = []
    var addrressComplete : [AddressComponent] = []
    var mUserPosition : CLLocationCoordinate2D!
    var predictionPresenter : PredictionAddressPresenter!
    var mGeocodePresenter : GeocoderPresenter!
    var mMapLocationSearchPresenter : MapLocationSearchPresenter!
    var locationManager: CLLocationManager!
    var lat: Double?
    var lon: Double?
    let marker = GMSMarker()
    var firstCoordinates : Bool?
    
    var mAddressDataSource : BaseDataSource<PredictionAddress, ItemAddresTableViewCell>!
    
    @IBOutlet weak var mHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var mPlazaLabel: UILabel!
    @IBOutlet weak var mZonaLabel: UILabel!
    @IBOutlet weak var mClusterLabel: UILabel!
    @IBOutlet weak var mDistritoLabel: UILabel!
    
    @IBOutlet weak var mContainerNoCoverageView: UIView!
    @IBOutlet weak var mContainerCoverageView: UIView!

    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstCoordinates = true
        mSearchTextField.delegate = self
        
        mAddressDataSource = BaseDataSource(tableView: mListAddressTableView, delegate: self)
        setViews()
    
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.title = "Validar Cobertura"
        getLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getLocation(){
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.startUpdatingLocation()
    }
    
    func setViews(){
        ViewUtils.setVisibility(view: mContainerCoverageView, visibility: .GONE)
        ViewUtils.setVisibility(view: mContainerNoCoverageView, visibility: .GONE)
        ViewUtils.setVisibility(view: mListAddressTableView, visibility: .GONE)
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let substring = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if(substring.characters.count > 4 ){
            searchAutocompleteDirection(substring)
        }
        return true
    }
    
    override func getPresenters() -> [BasePresenter]? {
        mMapLocationSearchPresenter = MapLocationSearchPresenter(viewController: self, mapLocationSearchDelegate: self)
        //mNoCoveragePresenter = NoCoveragePresenter(viewController: self)
        predictionPresenter = PredictionAddressPresenter(delegate: self)
        mGeocodePresenter = GeocoderPresenter(viewController: self, geocoderDelegate : self)
        return [mMapLocationSearchPresenter, predictionPresenter, mGeocodePresenter]
    }
    
    func searchAutocompleteDirection(_ substring: String) {
        autoComplete.removeAll(keepingCapacity: false)
        print(substring)
        predictionPresenter?.getPredictionAddress(keyword: substring as NSString)
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if(firstCoordinates)!{
            lat = locations[0].coordinate.latitude
            lon = locations[0].coordinate.longitude
            
            firstCoordinates = false
            
            let camera = GMSCameraPosition.camera(withLatitude: lat!, longitude: lon!, zoom: 17.0)
            mGoogleMapView.animate(to: camera)
            
            mUserPosition = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
            marker.position = mUserPosition
            marker.title = "Lavin House"
            marker.snippet = "México"
            marker.icon = UIImage(named: "ic_google_pin_empty")
            marker.map = mGoogleMapView
        }
    }
    
    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        ViewUtils.setVisibility(view: mListAddressTableView, visibility: .GONE)
        mSearchTextField.resignFirstResponder()
        let predictionAddress : PredictionAddress = item as! PredictionAddress
        predictionPresenter?.getLatLon(placeId: predictionAddress.place_id!)
        mSearchTextField.text = predictionAddress.description_!
    }
    
    func onSuccessPrediction(collectionAddress: [PredictionAddress]) {
        ViewUtils.setVisibility(view: mListAddressTableView, visibility: .NOT_GONE)
        mAddressDataSource.update(items: collectionAddress)
        mListAddressTableView.layoutIfNeeded()
        mHeightConstraint.constant = mListAddressTableView.contentSize.height
        mListAddressTableView.layoutIfNeeded()
    }
    
    func onSuccessGeometry(geoLatLon: Geometry, formatAddress: [AddressComponent]) {
        addrressComplete = formatAddress
        let camera = GMSCameraPosition.camera(withLatitude: (geoLatLon.location?.lat)!, longitude: (geoLatLon.location?.lng)!, zoom: 17.0)
        mGoogleMapView.animate(to: camera)
        
        mUserPosition = CLLocationCoordinate2D(latitude: (geoLatLon.location?.lat)!, longitude: (geoLatLon.location?.lng)!)
        marker.position = mUserPosition
        marker.icon = UIImage(named: "ic_google_pin_empty")
        marker.map = mGoogleMapView
        
        ViewUtils.setVisibility(view: self.mListAddressTableView, visibility: .GONE)
    }
    
    func onSuccessLoadAddress(mDirectionBean: DireccionBean) {
        //RoutingApp.OnConfirmLocation(viewController: self, coberturaResponse : mCoberturaResponse)
        
        
    }

    func onSuccessCoverage(mCoberturaResponse: CoberturaResponse) {
        
        ViewUtils.setVisibility(view: searchView, visibility: .GONE)
        ViewUtils.setVisibility(view: mContainerCoverageView, visibility: .NOT_GONE)
        
        mZonaLabel.text = mCoberturaResponse.feasibility?.zona
        mPlazaLabel.text = mCoberturaResponse.feasibility?.region
        mClusterLabel.text = mCoberturaResponse.feasibility?.name_Cluster
        mDistritoLabel.text = mCoberturaResponse.feasibility?.distric
       
    }
    
    func onErrorCoverageValidate() {
        ViewUtils.setVisibility(view: searchView, visibility: .GONE)
        ViewUtils.setVisibility(view: mContainerCoverageView, visibility: .GONE)
        ViewUtils.setVisibility(view: mContainerNoCoverageView, visibility: .NOT_GONE)
    }
    
    @IBAction func nextContinueButton(_ sender: Any) {
        var x  =  mUserPosition
        var lat = (x?.latitude)!
        var long = (x?.longitude)!

        mMapLocationSearchPresenter.validateCoverage(position: mGoogleMapView.camera.target)
        mGeocodePresenter.getAddressForLatLng(latitude: "\(lat)", longitude: "\(long)")
        
        
    
        
    }
    
    @IBAction func mContinueCovergaButton(_ sender: Any) {
        ViewControllerUtils.pushViewController(from: self, to: InstallationAddressViewController.self)
       // mGeocodePresenter.getAddressForLatLng(latitude:  , longitude: )
        
    }
    
    @IBAction func acceptButton(_ sender: Any) {
        ViewUtils.setVisibility(view: searchView, visibility: .NOT_GONE)
        ViewUtils.setVisibility(view: mContainerCoverageView, visibility: .GONE)
        ViewUtils.setVisibility(view: mContainerNoCoverageView, visibility: .GONE)
        
    }
    
    
    
    
    
}

