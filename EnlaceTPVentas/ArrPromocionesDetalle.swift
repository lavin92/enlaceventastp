//
//  ArrPromocionesDetalle.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 10/04/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ArrPromocionesDetalle: NSObject, Mappable {
    
    var id : String?
    var name : String?
    var cat_ChannelSale : String?
    var cat_City : String?
    var cat_Cluster : String?
    var cat_Plazo : String?
    var zipCode : String?
    var cat_typePayment : String?
    var dp_Promotion : String?
    var status : String?
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        
        id <- map["Id"]
        name <- map["Name"]
        cat_ChannelSale <- map["CAT_CanalVenta"]
        cat_City <- map["CAT_Ciudad"]
        cat_Cluster <- map["CAT_Cluster"]
        cat_Plazo <- map["CAT_Plazo"]
        zipCode <- map["CodigoPostal"]
        cat_typePayment <- map["CAT_TipoPago"]
        dp_Promotion <- map["DP_Promocion"]
        status <- map["Estatus"]

}

}
