//
//  GeocoderDelegate.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 12/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//


import UIKit
import SwiftBaseLibrary

public protocol GeocoderDelegate : NSObjectProtocol {
    func onSuccessLoadAddress(mDirectionBean : DireccionBean)
}

class GeocoderPresenter: BaseVentasPresenter {
    
    var mGeocoderDelegate : GeocoderDelegate!
    
    init(viewController: BaseViewController, geocoderDelegate : GeocoderDelegate){
        super.init(viewController: viewController)
        self.mGeocoderDelegate = geocoderDelegate
    }
    
    func getAddressForLatLng(latitude: String, longitude: String) {
        let url = NSURL(string: "\(ApiDefinition.GEO_CODING_BASE_URL)latlng=\(latitude),\(longitude)&key=\(ApiDefinition.GEO_CODING_API_KEY)")
        let data = NSData(contentsOf: url! as URL)
        let json = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
        if let result = json["results"] as? NSArray {
            if let addressComponent = result[0] as? [String:Any] {
                if let address = addressComponent["address_components"] as? [Any] {
                    let direccionBean : DireccionBean = DireccionBean()
                    if let arrayAddress = address[0] as? NSDictionary {
                        direccionBean.noExt = arrayAddress["short_name"] as! String
                    }
                    if let arrayAddress = address[1] as? NSDictionary {
                        direccionBean.street = arrayAddress["short_name"] as! String
                    }
                    if let arrayAddress = address[2] as? NSDictionary {
                        direccionBean.colony = arrayAddress["short_name"] as! String
                    }
                    if let arrayAddress = address[3] as? NSDictionary {
                        direccionBean.city = arrayAddress["short_name"] as! String
                    }
                    if let arrayAddress = address[4] as? NSDictionary {
                        direccionBean.delegation = arrayAddress["short_name"] as! String
                    }
                    if let arrayAddress = address[5] as? NSDictionary {
                        direccionBean.state = arrayAddress["short_name"] as! String
                    }
                   /*
                   if let arrayAddress = address[7] as? NSDictionary {
                    if  arrayAddress["short_name"] != nil {
                         direccionBean.zipCode = arrayAddress["short_name"] as! String
                    }
                    }*/
                    
                    mGeocoderDelegate.onSuccessLoadAddress(mDirectionBean: direccionBean)
                }
            }
        }
    }
    
    func loadAddressFromZipCode(zipCode : String){
        let cpRequest : AddressCPRequest = AddressCPRequest(cp: zipCode)
        RetrofitManager<AddressCpResponse>.init(requestUrl: ApiDefinition.API_CP, delegate: self).request(requestModel: cpRequest)
    }
    
    func successLoadAddress(requestUrl: String, addressCpResponse : AddressCpResponse){
        /*if addressCpResponse.resultObj?.result == "0" {
            //            mGeocoderDelegate.onSuccessLoadAddress(mDirecitonBean: addressCpResponse)
        } else {
            super.onErrorLoadResponse(requestUrl: requestUrl, messageError: "")
        }*/
    }
    
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        AlertDialog.hideOverlay()
        if (requestUrl == ApiDefinition.API_CP){
            successLoadAddress(requestUrl: requestUrl, addressCpResponse : response as! AddressCpResponse)
        }
    }
    
}

