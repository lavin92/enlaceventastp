//
//  DetallePlanResponse.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 31/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class DetallePlanResponse: BaseResponse {
    
    
    var result : Result?
    var arrServiciosIncluidos: [ArrServiciosIncluidos] = []
    var arrServiciosAdicionales: [ArrServiciosAdicionales] = []
    var arrProductosAdicionales: [ArrProductosAdicionales] = []
    var arrPromocionesPlan : [ArrPromocionesPlan] = []
    
    public required convenience init?(map: Map) {
        self.init()
        
    }
    
    
    public override func mapping (map: Map) {
        
        result <- map["Result"]
        arrServiciosIncluidos <- map["ArrServiciosIncluidos"]
        arrServiciosAdicionales <- map["ArrServiciosAdicionales"]
        arrProductosAdicionales <- map["ArrProductosAdicionales"]
        arrPromocionesPlan <- map["ArrPromocionesPlan"]
        
    }
    
}
