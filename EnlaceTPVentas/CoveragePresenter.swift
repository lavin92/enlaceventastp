//
//  CoveragePresenter.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 23/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

public protocol CoberturaDelegate : NSObjectProtocol {
    func onSuccessCobertura(feasibility : Feasibility)
    func onErrorCobertura()
}

class CoveragePresenter: BaseVentasPresenter {
    
    var mCoberturaDelegate : CoberturaDelegate!
    
    init(viewController: BaseViewController, coberturaDelegate : CoberturaDelegate) {
        super.init(viewController: viewController)
        mCoberturaDelegate = coberturaDelegate
    }
    
    func validateCoverage(idSite : String, nameSite : String, latitude : String, longitude : String, bandWidth : String, typeClient : String){
        let data : Datam = Datam()
        data.vIdSite = idSite
        data.vNameSite = nameSite
        data.vLatitude = latitude
        data.vLongitude = longitude
        data.vBandwidth = bandWidth
        data.vTypeClient = typeClient
        let coberturaResquest : CoberturaRequest = CoberturaRequest()
        coberturaResquest.validatecoverage = ValidateCoverage()
        coberturaResquest.validatecoverage?.mData = data
        
        RetrofitManager<CoberturaResponse>.init(requestUrl: ApiDefinition.WS_COBERTURA, delegate: self).request(requestModel: coberturaResquest)
    }
    
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        AlertDialog.hideOverlay()
        if requestUrl == ApiDefinition.WS_COBERTURA {
            let coberturaResponse : CoberturaResponse = response as! CoberturaResponse
            if coberturaResponse.results?.rCode == "0" {
                mCoberturaDelegate.onSuccessCobertura(feasibility: coberturaResponse.feasibility!)
                let feasibility = coberturaResponse.feasibility
                if feasibility != nil {
                    if (feasibility?.feasibility == ("1") || feasibility?.feasibility == ("2") || feasibility?.feasibility == ("6") || feasibility?.feasibility == ("7")  ){
                        
                        mCoberturaDelegate.onSuccessCobertura(feasibility: coberturaResponse.feasibility!)
                        }
                    else {
                        mCoberturaDelegate.onErrorCobertura()
                    }
                    
                   
            }else {
                
                onErrorLoadResponse(requestUrl: requestUrl, messageError: (coberturaResponse.results?.rCode)!)
                
            }
        }
    }
}

}
