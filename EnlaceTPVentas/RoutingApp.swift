//
//  RoutingApp.swift
//  EnlaceTPVentas
//
//  Created by Jorge Hdez VIlla on 22/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class RoutingApp: NSObject {
    
    static func OnSuccessUserLogin(viewController : UIViewController,nameEmployee: String){
        var extras : [String : AnyObject] = [:]
        extras ["name"] = nameEmployee as AnyObject
        ViewControllerUtils.presentViewControllerWithNavigation(from: viewController, to: WelcomeViewController.self, extras: extras)
    }
    
    
    static func OnConfirmLocation(viewController: UIViewController, coberturaResponse : CoberturaResponse){
        var extras : [String : AnyObject] = [:]
        //extras[KeysEnum.EXTRA_DIRECCION_BEAN] = coberturaResponse
        //ViewControllerUtils.pushViewController(from: viewController, to: ConfirmLocationViewController.self, extras : extras)
    }
    
}
