//
//  Salesman.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 29/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class Salesman: NSObject, Mappable {
    
    var ownerId : String = ""
    var channel : String = ""
    var subChannel : String = ""
    var folioContract : String = ""
    var idDevice : String = ""
    var userLogged : String = ""
    var originApp : String = ""
    var isCommissionable : String = ""
    var imei : String = ""
    var cellularModel : String = ""
    var expressSale : String = ""
    var approveExpressSale : String = ""
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    
    public func mapping(map: Map) {
        
        ownerId <- map ["OwnerId"]
        channel <- map ["Canal"]
        subChannel <- map ["SubCanal"]
        folioContract <- map ["FolioContrato"]
        idDevice <- map ["IdDispositivo"]
        userLogged <- map ["Usuario_Logueado"]
        originApp <- map ["OrigenApp"]
        isCommissionable <- map ["EsComisionable"]
        imei <- map ["IMEI"]
        cellularModel <- map ["ModeloCelular"]
        expressSale <- map ["VentaExpress"]
        approveExpressSale <- map ["AprobarVentaExpress"]
        
        
        
        
        
    }
}

