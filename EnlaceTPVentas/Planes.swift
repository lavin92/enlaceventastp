//
//  Planes.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 02/03/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class Planes: NSObject, Mappable {

    var invoiceAccountId : String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    public func mapping(map: Map) {
        
        invoiceAccountId <- map["CuentaFacturaId"]
        
    }
    
    
}
