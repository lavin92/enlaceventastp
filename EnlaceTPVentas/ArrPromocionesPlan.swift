//
//  ArrPromocionesPlan.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 13/03/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ArrPromocionesPlan: NSObject, Mappable {
    
   
    var id : String?
    var name : String?
    var commentary : String?
    var deescription : String?
    var plan : String?
    var changePlanGrill : String?
    var promotion : String?
    var status : String?
    var plazo : String?
    var statusPromotions : String?
    var commentaryPromotion : String?
    var namePromotion : String?
    var companyId : String?
    var isAutomatic : String?
    var isPostSales : String?
    var statusPromo : String?
    var isSalesNew : String?
    var endDate : String?
    var startDate: String?
    var idPromotion : String?
    var arrPromocionesDetalle: [ArrPromocionesDetalle] = []
    var arrDp_PromocionesServicioProducto : [ArrDP_PromocionesServicioProducto] = []
    
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    
    public func mapping(map: Map) {
        
        id <- map["Id"]
        name <- map["Nombre"]
        commentary <- map["Comentario"]
        deescription <- map ["Descripcion"]
        plan <- map ["Plan"]
        changePlanGrill <- map["PlanCambioParrilla"]
        promotion <- map["Promocion"]
        status <- map["Estatus"]
        plazo <- map["Plazo"]
        statusPromotions <- map["EstatusPromociones"]
        commentaryPromotion <- map["ComentarioPromocion"]
        namePromotion <- map["NombrePromocion"]
        companyId <- map ["CompaniaId"]
        isAutomatic <- map["EsAutomatica"]
        isPostSales <- map["EsPostVenta"]
        statusPromo <- map["EstatusPromocion"]
        isSalesNew <- map["EsVentaNueva"]
        endDate <- map["FechaFin"]
        startDate <- map["FechaInicio"]
        idPromotion <- map["IdPromocion"]
        arrPromocionesDetalle <- map["ArrPromocionesDetalle"]
        arrDp_PromocionesServicioProducto <- map["ArrDP_PromocionesServicioProducto"]
        
        

        
    }

}
