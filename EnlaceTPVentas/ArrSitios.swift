//
//  ArrSitios.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 30/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ArrSitios: NSObject, Mappable {
    
    var idSite : String = ""
    var street : String = ""
    var numberExterno : String = ""
    var numberExternoEditable : String = ""
    var numberInterno : String = ""
    var colony : String = ""
    var postalCode: String = ""
    var delegationMunicipality : String = ""
    var state : String = ""
    var city : String = ""
    var plaza : String = ""
    var district : String = ""
    var region : String = ""
    var regionId : String = ""
    var cluster : String = ""
    var sector : String = ""
    var feasibility : String = ""
    var coverage : String = ""
    var typeCoverage : String = ""
    var arrSitioPlan: [ArrSitioPlan] = []
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    public func mapping(map: Map) {
        
        idSite <- map ["IdSitio"]
        street <- map ["Calle"]
        numberExterno <- map ["NumeroExterior"]
        numberExternoEditable <- map ["NumeroExteriorEditable"]
        numberInterno <- map ["NumeroInterior"]
        colony <- map ["Colonia"]
        postalCode <- map ["CodigoPostal"]
        delegationMunicipality <- map ["DelegacionMunicipio"]
        state <- map ["Estado"]
        city <- map ["Ciudad"]
        plaza <- map ["Plaza"]
        district <- map ["Distrito"]
        region <- map ["Region"]
        regionId <- map ["RegionId"]
        cluster <- map ["Cluster"]
        sector <- map ["Zona"]
        feasibility <- map ["Factibilidad"]
        coverage <- map ["Cobertura"]
        typeCoverage <- map ["TipoCobertura"]
        arrSitioPlan <- map ["SitioPlan"]
        
       
    }
}

