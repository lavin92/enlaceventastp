//
//  Result.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 30/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class Result: NSObject, Mappable {

    var rResult : String?
    var rIdResult : String?
    var rDescription : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
    
        rResult <- map["Result"]
        rIdResult <- map["IdResult"]
        rDescription <- map["Description"]
    }
}
