//
//  ApprovedSales2ViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 11/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit

class ApprovedSales2ViewController: UIViewController {

    
    @IBOutlet weak var salesListTableView: UITableView!
    
    var arrayItems = [Sales]()
    var salesDataSources: SalesDataSources!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let obj: Sales = Sales()
        obj.tittle = "No. de contrato: R1874282829"
        obj.tittle2 = "Fecha de liberación:"
        obj.tittle3 = "13/08/2017"
        obj.tittle4 = "Estatus Mesa de Control:"
        obj.tittle5 = "Aprobada"
        obj.image = "icon_green_dot"
        arrayItems.append(obj)
        
        let obj1: Sales = Sales()
        obj1.tittle = "No. de contrato: R1874282829"
        obj1.tittle2 = "Fecha de liberación:"
        obj1.tittle3 = "13/08/2017"
        obj1.tittle4 = "Estatus Mesa de Control:"
        obj1.tittle5 = "Aprobada"
        obj1.image = "icon_green_dot"
        arrayItems.append(obj1)
        
        let obj2: Sales = Sales()
        obj2.tittle = "No. de contrato: R1874282829"
        obj2.tittle2 = "Fecha de liberación:"
        obj2.tittle3 = "13/08/2017"
        obj2.tittle4 = "Estatus Mesa de Control:"
        obj2.tittle5 = "Aprobada"
        obj2.image = "icon_green_dot"
        arrayItems.append(obj2)
        
        let obj3: Sales = Sales()
        obj3.tittle = "No. de contrato: R1874282829"
        obj3.tittle2 = "Fecha de liberación:"
        obj3.tittle3 = "13/08/2017"
        obj3.tittle4 = "Estatus Mesa de Control:"
        obj3.tittle5 = "Aprobada"
        obj3.image = "icon_green_dot"
        arrayItems.append(obj3)
        
        let obj4: Sales = Sales()
        obj4.tittle = "No. de contrato: R1874282829"
        obj4.tittle2 = "Fecha de liberación:"
        obj4.tittle3 = "13/08/2017"
        obj4.tittle4 = "Estatus Mesa de Control:"
        obj4.tittle5 = "Aprobada"
        obj4.image = "icon_green_dot"
        arrayItems.append(obj4)
    
    
        let nib = UINib(nibName: "CellSalesStatusTableViewCell", bundle:nil)
        salesListTableView.register(nib, forCellReuseIdentifier:"CellSalesStatusTableViewCell")
        salesDataSources = SalesDataSources(tableView: self.salesListTableView)
        salesDataSources?.update(coleccionInfo: arrayItems)
        self.salesListTableView.reloadData()
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
