//
//  SaveURLImagesEnlaceRequest.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 01/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class SaveURLImagesEnlaceRequest: NSObject, Mappable{

    var process : Process?
    var oportunityID : String = ""
    var urlImage : String = ""
    
    
    
    override init() {
        super .init()
    }
    
    public required init?(map: Map) {
        
    }
    
    internal func mapping(map: Map) {
        
        process <- map["process"]
        oportunityID <- map["OportunityID"]
        urlImage <- map["URLImage"]
    
}
}
