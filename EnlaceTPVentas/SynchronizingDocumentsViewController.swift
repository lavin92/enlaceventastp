//
//  SynchronizingDocumentsViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 05/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit

class SynchronizingDocumentsViewController: UIViewController {

    
    @IBOutlet weak var syncOKImage: UIImageView!
    @IBOutlet weak var synchronizingIDLabel: UILabel!
    @IBOutlet weak var synchronizingIDProgressView: UIProgressView!
    @IBOutlet weak var synchronizingContractProgressView: UIProgressView!
    @IBOutlet weak var synchronizingContractLabel: UILabel!
    @IBOutlet weak var syncLoadImage: UIImageView!
    @IBOutlet weak var synchronizingID2ProgressView: UIProgressView!
    @IBOutlet weak var errorSyncLabel: UILabel!
    @IBOutlet weak var syncErrorImage: UIImageView!
    @IBOutlet weak var synchronizingVoucherProgressView: UIProgressView!
    @IBOutlet weak var syncVoucherLabel: UILabel!
    @IBOutlet weak var syncVoucherImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) {
            (timer:Timer) in
            
            self.synchronizingContractProgressView.setProgress(self.synchronizingContractProgressView.progress + 10, animated: true)
            
            if self.synchronizingContractProgressView.progress >= 1 {
                timer.invalidate()
            }
            
            timer.fire()
    }
        
        let time = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) {
            (timer:Timer) in
            
            self.synchronizingIDProgressView.setProgress(self.synchronizingIDProgressView.progress + 10, animated: true)
            
            if self.synchronizingIDProgressView.progress >= 1 {
                timer.invalidate()
            }
            
            timer.fire()
        }
        
        let tiemp = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) {
            (timer:Timer) in
            
            self.synchronizingID2ProgressView.setProgress(self.synchronizingID2ProgressView.progress + 10, animated: true)
            
            if self.synchronizingID2ProgressView.progress >= 1 {
                timer.invalidate()
            }
            
            timer.fire()
        }
        
        let tim = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) {
            (timer:Timer) in
            
            self.synchronizingVoucherProgressView.setProgress(self.synchronizingVoucherProgressView.progress + 10, animated: true)
            
            if self.synchronizingVoucherProgressView.progress >= 1 {
                timer.invalidate()
            }
            
            timer.fire()
        }

   
    }
    
    /*@IBAction func acceptButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
        present(myVC, animated: true, completion: nil)

    }*/
    
}

