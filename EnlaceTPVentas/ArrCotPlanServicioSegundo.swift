//
//  ArrCotPlanServicioSegundo.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 30/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ArrCotPlanServicioSegundo: NSObject, Mappable {
    
    var dp_PlanService : String = ""
    var isServiceAdditional : String = ""
    var nameService : String = ""
    var type : String = ""
    var dp_PromotionPlan : String = ""
    var arrCotServicioProducto: [ArrCotServicioProducto] = []
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    public func mapping(map: Map) {
        
        dp_PlanService <- map ["DP_PlanServicio"]
        isServiceAdditional <- map ["EsServicioAdicional"]
        nameService <- map ["NombreServicio"]
        type <- map ["Tipo"]
        dp_PromotionPlan <- map ["DP_PromocionPlan"]
        arrCotServicioProducto <- map ["Cot_ServicioProducto"]
        
    }
}

