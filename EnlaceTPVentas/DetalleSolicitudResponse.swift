//
//  DetalleSolicitudResponse.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 31/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class DetalleSolicitudResponse: NSObject, Mappable {

    var result : Result?
    var numResult : String = ""
    var arrDetalleSolicitud: [ArrDetalleSolicitud] = []
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        
        result <- map["Result"]
        numResult <- map["NumResultados"]
        arrDetalleSolicitud <- map["ArrDetalleSolicitud"]
        
        
    }
}
