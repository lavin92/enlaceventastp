//
//  DP_ServicioProducto.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 13/03/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class DP_ServicioProducto: NSObject, Mappable {

    var id : String?
    var name : String?
    var groupAddon : String?
    var isAutomaticCity : String?
    var isChargeOnly: String?
    var status : String?
    var isVisible : String?
    var idBrmCU: String?
    var idBrmForward : String?
    var ieps : String?
    var addMaximum : String?
    var nameEditable : String?
    var planDiscountId : String?
    var plazo : String?
    var priceBase : String?
    var priceEditable : String?
    var priceSoonPayment : String?
    var typeProduct : String?
    var typeProductText : String?
    var idProduct : String?
    var nameProduct : String?
    var productId : String?
    var hasDynamicIp : String?
    var hasFijaIp : String?
    var hasSTBAditional : String?
    var isCCVT : String?
    var isWifi : String?
    var quantity : String?
    var statusProduct : String?
    var startDate : String?
    var endDate : String?
    var commentaryProduct : String?
    var isSoonPayment : String?
    var idPromotionSP : String?
    
    
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    public func mapping(map: Map) {
    
        id <- map["Id"]
        name <- map["Nombre"]
        groupAddon <- map["AgrupacionAddon"]
        isAutomaticCity <- map["EsAutomaticoCiudad"]
        isChargeOnly <- map["EsCargoUnico"]
        status <- map["Estatus"]
        isVisible <- map["EsVisible"]
        idBrmCU <- map["IdBrmCU"]
        idBrmForward <- map["IdBrmForward"]
        ieps <- map["IEPS"]
        addMaximum <- map["MaximoAgregar"]
        nameEditable <- map["NombreEditable"]
        planDiscountId <- map["PlanDescuentoId"]
        plazo <- map["Plazo"]
        priceBase <- map["PrecioBase"]
        priceEditable <- map["PrecioEditable"]
        priceSoonPayment <- map["PrecioProntoPago"]
        typeProduct <- map["TipoProducto"]
        typeProductText <- map ["TipoProductoTexto"]
        idProduct <- map ["IdProducto"]
        nameProduct <- map["NameProducto"]
        productId <- map["ProductoId"]
        hasDynamicIp <- map["TieneIPDinamica"]
        hasFijaIp <- map["TieneIPFija"]
        hasSTBAditional <- map ["TieneSTBAdicional"]
        isCCVT <- map["EsCCVT"]
        isWifi <- map["EsWifi"]
        quantity <- map["Cantidad"]
        statusProduct <- map["EstatusProducto"]
        startDate <- map["FechaInicio"]
        endDate <- map["FechaFin"]
        commentaryProduct <- map["ComentarioProducto"]
        isSoonPayment <- map["EsProntoPago"]
        idPromotionSP <- map["IdPromocionSP"]
        
    }
    
}
