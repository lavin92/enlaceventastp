//
//  CoberturaResponse.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 31/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary


class CoberturaResponse: BaseResponse {
    
    var results : Results?
    var validateCoverageResponse : ValidateCoverageResponse?
    var feasibility : Feasibility?
    var responseDetail : ResponseDetail?
    
    
    
    public required convenience init?(map: Map){
        self.init()
    }
    
    
    public override func mapping (map: Map) {
        
        results <- map ["Results"]
        validateCoverageResponse <- map ["ValidateCoverageResponse"]
        feasibility <- map ["Factibilidad"]
        responseDetail <- map ["Detalle_Respuesta"]
        
    }
}
