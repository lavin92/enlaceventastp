//
//  InstallationAddressViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 21/09/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import TextFieldEffects

class InstallationAddressViewController: BaseViewController {
    
    let mValidator = UtilsTp()
    
    
    @IBOutlet weak var postalCodeTextField: HoshiTextField!
    @IBOutlet weak var streetTextField: HoshiTextField!
    @IBOutlet weak var numberExternoTextField: HoshiTextField!
    @IBOutlet weak var numberInternoTextField: HoshiTextField!
    @IBOutlet weak var colonyTextField: HoshiTextField!
    @IBOutlet weak var stateTextField: HoshiTextField!
    @IBOutlet weak var cityTextField: HoshiTextField!
    @IBOutlet weak var delegamuniTextField: HoshiTextField!
    @IBOutlet weak var betweenStreetTextField: HoshiTextField!
    @IBOutlet weak var andStreetTextField: HoshiTextField!
    @IBOutlet weak var observationsTextField: HoshiTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberExternoTextField.delegate = self
        numberInternoTextField.delegate = self
        postalCodeTextField.delegate =  self
        streetTextField.delegate = self
        colonyTextField.delegate = self
        stateTextField.delegate = self
        cityTextField.delegate = self
        delegamuniTextField.delegate = self
        betweenStreetTextField.delegate = self
        andStreetTextField.delegate = self
        observationsTextField.delegate = self
        self.navigationItem.title = "Dirección de Instalación"
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      
        if  textField == postalCodeTextField {
            return mValidator.numbersAndLength(textField, shouldChangeCharactersIn: range, replacementString: string, maxChar: 5)
        }
      
        return true
    }
    
    
   
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    
     override func viewDidAppear(_ animated: Bool) {
        
        
    }

    override func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
 
        return false
    }

    @IBAction func validateCoverageButton(_ sender: Any) {
        
        let textField1 = mValidator.textFieldDidChange(textField: postalCodeTextField!)
        let textField2 = mValidator.textFieldDidChange(textField: streetTextField!)
        let textField3 = mValidator.textFieldDidChange(textField: numberExternoTextField!)
        let textField4 = mValidator.textFieldDidChange(textField: colonyTextField!)
        let textField5 = mValidator.textFieldDidChange(textField: stateTextField!)
        let textField6 = mValidator.textFieldDidChange(textField: cityTextField!)
        let textField7 = mValidator.textFieldDidChange(textField: delegamuniTextField!)
        let textField8 = mValidator.textFieldDidChange(textField: betweenStreetTextField!)
        let textField9 = mValidator.textFieldDidChange(textField: andStreetTextField!)
        let textField10 = mValidator.textFieldDidChange(textField: observationsTextField!)
        
    if((textField1)&&(textField2)&&(textField3)&&(textField4)&&(textField5)&&(textField6)&&(textField7)&&(textField8)&&(textField9)&&(textField10)) {

            ViewControllerUtils.pushViewController(from: self, to: DataContactViewController.self)
            
           }
        else{
            AlertDialog.show(title: "Error", body: "Favor de Llenar Todos los Campos ", view: self)
            
        }
       
        }
    
    
    }
    
        


