//
//  InfoUser.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 13/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class InfoUser: Object, Mappable {

    dynamic var uuid : String = UUID().uuidString
    dynamic var iUserType : String?
    dynamic var iName: String?
    dynamic var iTipoC : String?
    dynamic var iUserRollID : String?
    dynamic var inEmployeeC : String?
    dynamic var iAccountId : String?
    dynamic var iEmail : String?
    dynamic var iGrupoC : String?
    dynamic var iIsActive : String?
    dynamic var iId : String?
    dynamic var iLastName : String?
    dynamic var iGrupoAsignadoC : String?
    
    required convenience init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
        iUserType <- map["UserType"]
        iName <- map["Name"]
        iTipoC <- map["TipoC"]
        iUserRollID <- map["UserRollID"]
        inEmployeeC <- map["NoEmpleadoC"]
        iAccountId <- map["AccountId"]
        iEmail <- map["Email"]
        iGrupoC <- map["GrupoC"]
        iIsActive <- map["IsActive"]
        iId <- map["Id"]
        iLastName <- map["LastName"]
        iGrupoAsignadoC <- map["GrupoAsignadoC"]
    }
    
    override public static func primaryKey() -> String? {
        return "uuid"
    }

}
