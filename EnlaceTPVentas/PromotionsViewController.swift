//
//  PromotionsViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 15/03/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit

class PromotionsViewController: UIViewController {

    
    @IBOutlet weak var promotionsTableView: UITableView!
    
    var arrayItems = [List]()
    var listDataSource : ListDataSources!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let obj: List = List()
        obj.tittle = "Promoción Agosto"
        //obj.image = "photo_01"
        arrayItems.append(obj)
        
        let obj1: List = List()
        obj1.tittle = "Promoción 500 GB"
        arrayItems.append(obj1)
        
        let obj2: List = List()
        obj2.tittle = "Promoción SuperInternet Plus 500"
        arrayItems.append(obj2)
        
        let obj3: List = List()
        obj3.tittle = "Promoción fin de mes"
        arrayItems.append(obj3)
        
        
        
        
        let nib = UINib(nibName: "CellAdditionalSettingsTableViewCell", bundle: nil)
        promotionsTableView.register(nib, forCellReuseIdentifier: "CellAdditionalSettingsTableViewCell")
        listDataSource = ListDataSources(tableView: self.promotionsTableView)
        listDataSource?.update(coleccionInfo: arrayItems)
        self.promotionsTableView.reloadData()
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
