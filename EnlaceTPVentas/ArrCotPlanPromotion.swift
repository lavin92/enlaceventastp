//
//  ArrCotPlanPromotion.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 30/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ArrCotPlanPromotion: NSObject, Mappable {
    
    var id_DP_PromocionPlan : String = ""
    
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    public func mapping(map: Map) {
        
        id_DP_PromocionPlan <- map ["Id_DP_PromocionPlan"]
        
    }
    
}

