//
//  GeometryResponse.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 12/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

public class GeometryResponse : NSObject, Mappable{
    
    var result: ResultLocation?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        result        <- map["result"]
    }
    
}
