//
//  Sitios.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 02/03/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class Sitios: NSObject, Mappable {

    var siteId : String = ""
    var planes : [Planes] = []
    var cotSitio : [Cot_Sitio] = []
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    public func mapping(map: Map) {
        
        siteId <- map["SitioId"]
        planes <- map["Planes"]
        cotSitio <- map["Cot_Sitio"]
    
    }
    
    
}
