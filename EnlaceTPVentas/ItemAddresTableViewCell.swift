//
//  ItemAddresTableViewCell.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 12/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit

class ItemAddresTableViewCell: BaseTableViewCell{

 
    @IBOutlet weak var mDescriptionAddressLabel: UILabel!
    
    
    @IBOutlet weak var mTitleAddressLabel: UILabel!
    
    override func pupulate(object :NSObject) {
        let prediction : PredictionAddress = object as! PredictionAddress
        mDescriptionAddressLabel.text = prediction.terms[0].value
        mTitleAddressLabel.text = prediction.description_
    }
    
    override func toString() -> String{
        return "ItemAddresTableViewCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
