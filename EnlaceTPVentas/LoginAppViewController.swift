//
//  LoginAppViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 25/09/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import LocalAuthentication

class LoginAppViewController: BaseViewController, LoginDelegate {
    func onSuccessLogin(nameEmployee: String) {
        RoutingApp.OnSuccessUserLogin(viewController: self,nameEmployee: nameEmployee)
    }
    
    
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var enployeeTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    var iconClick : Bool!
    
    var mFormValidator : FormValidator!
    var mLoginPresenter : LoginPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        iconClick = true
        
        mFormValidator = FormValidator(showAllErrors: true)
        mFormValidator.addValidators(validators:
            TextFieldValidator(textField: enployeeTextField, regex: RegexEnum.NOT_EMPTY),
            TextFieldValidator(textField: passwordTextField, regex: RegexEnum.NOT_EMPTY)
            )
        
        //enployeeTextField.text = "15005976"
        //passwordTextField.text = "TotalplayTest123"
        if (UserDefaults.standard.object(forKey: "username") != nil){
           IDTouch()
        }
        
    }
    
    override func getPresenter() -> BasePresenter? {
        mLoginPresenter = LoginPresenter(viewController: self, loginDelegate: self)
        return mLoginPresenter
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let x = UserDefaults.standard.object(forKey: "username") as?
            String{
            enployeeTextField.text = x
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func toEnterButton(_ sender: Any) {
        if mFormValidator.isValid() {
            let username = enployeeTextField.text?.trim()
            let password = passwordTextField.text?.trim()
            mLoginPresenter.login(username: username!, password: password!)
            
        }
        login()
       
    }
    
    
    @IBAction func visiblePasswordButton(_ sender: Any) {
        if (iconClick){
            passwordTextField.isSecureTextEntry = false
            iconClick = false
        }else {
            passwordTextField.isSecureTextEntry = true
            iconClick = true
        }
        
    }
    
    func IDTouch(){
        let context: LAContext = LAContext()
        
        if(context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)){
            var localizedReason = "Plase verify yourself"
            
            if #available(iOS 11.0, *){
                switch context.biometryType{
                case.faceID: localizedReason = "Unlock using Face ID "
                case.touchID : localizedReason = "Unlock using Touch ID "
                case.none: print("No Biometric support")
                }
                
            }else{
                
            }
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,localizedReason: localizedReason,reply: {(wasSuccesful,error) -> Void in
                if (wasSuccesful){
                    DispatchQueue.main.sync {
                        if let y = UserDefaults.standard.object(forKey: "password") as? String{
                           self.passwordTextField.text = y
                            self.login()
                        }
                    }
                }
                
            })
            
        }
    }
  
    @IBAction func actDoLogin(_ sender: Any) {
        login() 
    }
    func login(){
        if enployeeTextField.text == " " || passwordTextField.text == ""{
            AlertDialog.show(title: "Error ", body: "Favor de Llenar Todos los Campos", view: self)
        }
        else {
            mLoginPresenter.login(username: enployeeTextField.text!, password: passwordTextField.text!)
        }
        
    }
    
    
}

