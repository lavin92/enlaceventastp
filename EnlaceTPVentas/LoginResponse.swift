//
//  LoginResponse.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 13/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

public class LoginResponse: BaseResponse {
    
    var response : Response?
    var infoUser : InfoUser?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public override func mapping(map: Map) {
        response <- map["Response"]
        infoUser <- map["InfoUser"]
    }
    
    
    
    
}
