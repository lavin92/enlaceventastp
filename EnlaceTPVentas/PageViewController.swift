//
//  PageViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 29/09/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//


import UIKit
import Foundation
import SlidingContainerViewController
import SwiftBaseLibrary

class PageViewController: BaseViewController ,SlidingContainerViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Configurar Adicionales"
        //navigationItem.title = "Test Tabs"
        /*navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.font: UIFont(name: "HelveticaNeue-Light", size: 20)!
        ]*/
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let vc1 = viewController(viewController: ServiceViewController.self) as! ServiceViewController
        let vc2 = viewController(viewController: ProductsViewController.self) as! ProductsViewController
       // let vc3 = viewController(viewController: PromotionsViewController.self) as! PromotionsViewController
        
        let slidingContainerViewController = SlidingContainerViewController (
            parent: self,
            contentViewControllers: [vc1, vc2],
            titles: ["Servicios", "Productos"])
        
        
        
        view.addSubview(slidingContainerViewController.view)
        
        
        slidingContainerViewController.sliderView.appearance.outerPadding = 0
        slidingContainerViewController.sliderView.appearance.innerPadding = 50
        slidingContainerViewController.sliderView.appearance.selectorColor = UIColor(red: 44/255, green: 90/255, blue: 228/255, alpha: 1.0)
        slidingContainerViewController.sliderView.appearance.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.9)
        slidingContainerViewController.sliderView.appearance.fixedWidth = true
        slidingContainerViewController.sliderView.appearance.selectedTextColor = UIColor(red: 25.0/255.0, green: 72.0/255.0, blue: 179.0/255.0, alpha: 1)
        slidingContainerViewController.sliderView.appearance.textColor = UIColor(red: 44/255, green: 90/255, blue: 228/255, alpha: 1.0)
        slidingContainerViewController.setCurrentViewControllerAtIndex(0)
        slidingContainerViewController.delegate = self
    }
    
    func viewController(viewController: UIViewController.Type) -> UIViewController {
        let nameController : NSString = NSStringFromClass(viewController.classForCoder()) as NSString
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: nameController.components(separatedBy: ".").last!)
        return vc
    }
    
    // MARK: SlidingContainerViewControllerDelegate
    
    func slidingContainerViewControllerDidMoveToViewController(_ slidingContainerViewController: SlidingContainerViewController, viewController: UIViewController, atIndex: Int) {
        
    }
    
    func slidingContainerViewControllerDidShowSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
    
    func slidingContainerViewControllerDidHideSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
    
}

