//
//  AddressCpResponse.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 13/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

public class AddressCpResponse: BaseResponse {
    
    dynamic var arrColony : [ArrColony]?
    
    public required init?(map: Map) {
        
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        self.arrColony   <- map["ArrColonias"]
    }
    
}
