//
//  AddressComponent.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 12/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

public class AddressComponent : NSObject, Mappable{
    
    var long_name: String?
    var short_name : String?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        long_name        <- map["long_name"]
        short_name <- map["short_name"]
    }
}

