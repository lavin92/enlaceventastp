//
//  Login.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 13/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//  ObjectMapper es un framework escrito en Swift que hace que sea fácil para usted convertir sus objetos modelo (clases y estructuras) hacia y desde JSON.

import UIKit
import ObjectMapper

//Para admitir el mapeo, una clase o estructura solo necesita implementar el protocolo Mappable que incluye las siguientes funciones:

class Login: NSObject, Mappable {
    
    var user : String?
    var password : String?
    var ip : String?
    var input : String?
    required convenience init?(map: Map) {
        self.init()
    }
    
    //ObjectMapper usa el operador <- para definir cómo se mapea cada variable miembro a JSON y desde esta.
    
    public func mapping(map: Map) {
        user <- map["User"]
        password <- map["Password"]
        ip <- map["Ip"]
        input <- map["input"]
    }
    
}

