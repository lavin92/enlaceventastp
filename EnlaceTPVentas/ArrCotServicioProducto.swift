//
//  ArrCotServicioProducto.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 30/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ArrCotServicioProducto: NSObject,Mappable {

    var id_Cot_ServiceProduct : String = ""
    var discount : String = ""
    var dp_PromotionServiceProduct : String = ""
    var dp_ServiceProduct : String = ""
    var isChargerOnly : String = ""
    var isDiscount : String = ""
    var isProductAdditional : String = ""
    var isSoonPayment : String = ""
    var nameProduct : String = ""
    var parentProduct : String = ""
    var typeProduct : String = ""
    var quantity : String = ""
    var impuesto2 : String = ""
    var impuesto1 : String = ""
    var priceUnitBase : String = ""
    var priceUnit : String = ""
    var priceUnit_SoonPayment : String = ""
    var dp_PromotionPlan : String = ""
    
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    public func mapping(map: Map) {

        id_Cot_ServiceProduct <- map ["Id_Cot_ServicioProducto"]
        discount <- map ["Descuento"]
        dp_PromotionServiceProduct <- map ["DP_PromocionServicioProducto"]
        dp_ServiceProduct <- map ["DP_ServicioProducto"]
        isChargerOnly <- map ["EsCargoUnico"]
        isDiscount <- map ["EsDescuento"]
        isProductAdditional <- map ["EsProductoAdicional"]
        isSoonPayment <- map ["EsProntoPago"]
        nameProduct <- map ["NombreProducto"]
        parentProduct <- map ["ProductoPadre"]
        typeProduct <- map ["TipoProducto"]
        quantity <- map ["Cantidad"]
        impuesto2 <- map ["Impuesto2"]
        impuesto1 <- map ["Impuesto1"]
        priceUnitBase <- map ["PrecioUnitarioBase"]
        priceUnit <- map ["PrecioUnitario"]
        priceUnit_SoonPayment <- map ["PrecioUnitario_ProntoPago"]
        dp_PromotionPlan <- map ["DP_PromocionPlan"]

}
}
