//
//  DireccionBean.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 13/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//
import UIKit
import ObjectMapper
import RealmSwift

public class DireccionBean  : Object, Mappable {
    
    dynamic var uuid : String = UUID().uuidString
    dynamic var zipCode : String = ""
    dynamic var colony : String = ""
    dynamic var delegation : String = ""
    dynamic var city : String = ""
    dynamic var state : String = ""
    dynamic var street : String = ""
    dynamic var noExt : String = ""
    dynamic var noInt : String = ""
    dynamic var betweenStreets : String = ""
    dynamic var ReferenciaUrbana : String = ""
    dynamic var authorizationWhitoutCoverage : String = ""
    dynamic var cluster : String = ""
    dynamic var place : String = ""
    dynamic var district : String = ""
    dynamic var feasibility : String = ""
    dynamic var feasible : String = ""
    dynamic var typeCoverage : String = ""
    dynamic var region : String = ""
    dynamic var idRegion : String = ""
    dynamic var zoneValue : String = ""
    dynamic var latitude : String = ""
    dynamic var longitude : String = ""
    
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        
        zipCode        <- map["CodigoPostal"]
        colony        <- map["Colonia"]
        delegation        <- map["Delegacion"]
        city        <- map["Ciudad"]
        state        <- map["Estado"]
        street        <- map["Calle"]
        noExt        <- map["NumeroExterior"]
        noInt        <- map["NumeroInterior"]
        betweenStreets        <- map["EntreCalles"]
        ReferenciaUrbana        <- map["ReferenciaUrbana"]
        authorizationWhitoutCoverage        <- map["AutorizacionSinCobertura"]
        cluster        <- map["Cluster"]
        place        <- map["Plaza"]
        district        <- map["Distrito"]
        feasibility        <- map["Factibilidad"]
        feasible        <- map["Factible"]
        typeCoverage        <- map["TipoCobertura"]
        region        <- map["Region"]
        idRegion        <- map["RegionId"]
        zoneValue        <- map["zona"]
        latitude        <- map["Latitude"]
        longitude        <- map["Longitude"]
    }
    
    override public static func primaryKey() -> String? {
        return "uuid"
    }
    
}

