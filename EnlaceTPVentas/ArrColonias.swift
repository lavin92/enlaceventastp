//
//  ArrColonias.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 13/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper


class ArrColonias: NSObject, Mappable {
    
    var id : String?
    var name : String?
    var colony : String?
    var city : String?
    var key : String?
    var cluster : String?
    var delegationMunicipality : String?
    var state : String?
    var idStateEn : String?
    var idStateTP : String?
    var idMunicipalityEn : String?
    var idMunicipalityTP : String?
    var idOrigin : String?
    var idPopulationEN : String?
    var idPopulationTP : String?
    var idRegionEN : String?
    var idRegionTP : String?
    var limit : String?
    var limit1 : String?
    
    
    
   
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    
    public func mapping(map: Map) {

        id <- map["Id"]
        name <- map["Nombre"]
        colony <- map["Colonia"]
        city <- map["Ciudad"]
        key <- map["Clave"]
        cluster <- map["Cluster"]
        delegationMunicipality <- map["DelegacionMunicipio"]
        state <- map["Estado"]
        idStateEn <- map["IdEstadoEN"]
        idStateTP <- map["IdEstadoTP"]
        idMunicipalityEn <- map["IdMunicipioEN"]
        idMunicipalityTP <- map["IdMinicipioTP"]
        idOrigin <- map["idOrigen"]
        idPopulationEN <- map["IdPoblacionEN"]
        idPopulationTP <- map["IdPoblacionTP"]
        idRegionEN <- map["IdRegionEN"]
        idRegionTP <- map["IdRegionTP"]
        limit <- map["Limite"]
        limit1 <- map["Limite1"]
        
        
        
     
        
        
  }
}
