//
//  Data.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 31/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper


class Datam: NSObject, Mappable {

    var vIdSite : String?
    var vNameSite : String?
    var vLatitude : String?
    var vLongitude : String?
    var vBandwidth : String?
    var vTypeClient : String?
    
    
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    override init() {
        
    }
    
    init(vIdSite : String, vNameSite : String, vLatitude : String, vLongitude : String, vBandwidth : String, vTypeClient : String) {
        self.vIdSite = vIdSite
        self.vNameSite = vNameSite
        self.vLatitude = vLatitude
        self.vLongitude = vLongitude
        self.vBandwidth = vBandwidth
        self.vTypeClient = vTypeClient
    }
    
    public func mapping(map: Map) {
        vIdSite <- map["idSitio"]
        vNameSite <- map["nombreSitio"]
        vLatitude <- map["latitude"]
        vLongitude <- map["longitude"]
        vBandwidth <- map["bandwidth"]
        vTypeClient <- map["tipoCliente"]
    }
}
