//
//  ContractDataViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 27/09/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class ContractDataViewController: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    let imagePicker : UIImagePickerController = UIImagePickerController()
    
    
    var num:String?
    var imagePickerController : UIImagePickerController!
   
    @IBOutlet weak var mCaratulaImage1: UIImageView!
    @IBOutlet weak var mCaratulaImage2: UIImageView!
    @IBOutlet weak var mFrontImage: UIImageView!
    @IBOutlet weak var mBackImagen: UIImageView!
    @IBOutlet weak var mOtherIdentification: UIImageView!
    @IBOutlet weak var mFront2Image: UIImageView!
    @IBOutlet weak var mBack2Imagen: UIImageView!
    @IBOutlet weak var mLeaf1Image: UIImageView!
    @IBOutlet weak var mLeaf2Image: UIImageView!
    @IBOutlet weak var mLeaf3Image: UIImageView!
    @IBOutlet weak var mCaratulaImage3: UIImageView!
    @IBOutlet weak var mCaratulaImage4: UIImageView!
    
    
    
    
    
    
    //@IBOutlet weak var firmElectronicView: UIView!
    @IBOutlet weak var mViewIdentificationContract: UIView!
    //@IBOutlet weak var captureFirmImageView: UIImageView!
    @IBOutlet weak var useIdentificationButton: CheckBoxButton!
    //@IBOutlet weak var captureFirmView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
      imagePicker.delegate = self
        
        let myColor = UIColor.black
       // captureFirmView.layer.borderColor = myColor.cgColor
       //captureFirmView.layer.borderWidth = 1.0
    }
    
    
    @IBAction func useIdentificationButton(_ sender: CheckBoxButton) {
    
        if (useIdentificationButton.isChecked){
            ViewUtils.setVisibility(view: mViewIdentificationContract, visibility: .GONE)
        }else{
            
            ViewUtils.setVisibility(view: mViewIdentificationContract, visibility: .NOT_GONE)
        }
        
    }
    
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func mask1CameraButton(_ sender: Any) {
        self.alertTakePhoto(value: "0")
    
        
    }
    
    
    @IBAction func mask2CameraButton(_ sender: Any) {
        self.alertTakePhoto(value: "1")
    
       
        }
    @IBAction func frontCameraButton(_ sender: Any) {
        self.alertTakePhoto(value: "2")
        
    }
    
    
    @IBAction func reverseCameraButton(_ sender: Any) {
        self.alertTakePhoto(value: "3")
        
    }
    
    
    @IBAction func otherIdentificationCamera(_ sender: Any) {
        self.alertTakePhoto(value: "4")
    }
    
    
    @IBAction func front2CameraButton(_ sender: Any) {
        
    }
    
    
    @IBAction func reverse2CameraButton(_ sender: Any) {
    }
    
    
    @IBAction func leaf1CameraButton(_ sender: Any) {
        
    }
    
    
    @IBAction func leaf2CameraButton(_ sender: Any) {
        
    }
    
    @IBAction func leaf3CameraButton(_ sender: Any) {
        
    }
    
    @IBAction func mask3CameraButton(_ sender: Any) {
    
    }
    
    @IBAction func mask4CameraButton(_ sender: Any) {
    
    }
    
    @IBAction func continueButton(_ sender: Any) {
        
    }
    
    
    /******* aqui esta la funcion para tomar la foto , o abrir la galeria , y esta el alert donde muestra las dos opciones :tomar foto , o abrir de galeria *********/
    func takePhoto(number: String){
        num = number
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = .camera;
            self.present(imagePickerController, animated: true, completion: nil)
        }
        
    }
    // tomar la foto de la galeria
    func takeGalery(number: String){
        num = number
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = .photoLibrary;
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    
    // view alert para tomar la foto o abrir la galeria
    func alertTakePhoto(value: String){
        let alert = UIAlertController(title: "Elige una opción", message: "", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Tomar foto", style: .default, handler: { (action) -> Void in
            self.takePhoto(number: value)
        })
        let action2 = UIAlertAction(title: "Abrir de Galeria", style: .default, handler: { (action) -> Void in
            self.takeGalery(number: value)
        })
        alert.view.tintColor = UIColor.black
        alert.addAction(action1)
        alert.addAction(action2)
        
        present(alert, animated: true, completion:{
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        })
    }
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePickerController.dismiss(animated: true, completion: nil)
        switch num {
        case "0":
            // viewImageContrato es el nombre de la imagen bindeada donde va a ir la foto
            mCaratulaImage1.image = info[UIImagePickerControllerOriginalImage] as? UIImage // solo necesitas eso en cada switch
        

        default:
            break
        }
    }
    
    func alertControllerBackgroundTapped(){   // esta funcion es para cerrar el view alert cuando des clic fuera del view alert.
        self.dismiss(animated:true, completion: nil)
    }
    
    /*@IBAction func firmButton(_ sender: Any) {
    ViewControllerUtils.presentViewControllerWithResult(from: self, to: FirmElectronicViewController.self, request: "")

    }*/
    
   /* func viewControllerForResult(keyRequest:String,result:ViewControllerResult,data:[String:AnyObject]){
        
        if (result == .RESULT_OK){
            if(data["Signature"] != nil){
                let img: UIImage = data ["Signature"] as! UIImage
                captureFirmImageView.image = img
            }
        }
    }*/
    
}


