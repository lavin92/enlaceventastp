//
//  DirectionResponse.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 12/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

public class DirectionResponse : NSObject, Mappable{
    
    var predictionData: [PredictionAddress] = []
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        predictionData        <- map["predictions"]
    }
    
}
