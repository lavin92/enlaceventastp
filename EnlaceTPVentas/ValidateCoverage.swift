//
//  ValidateCoverage.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 30/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ValidateCoverage: NSObject, Mappable {
    
    var mData : Datam?

    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
      mData <- map["Data"]
    }
    
}
