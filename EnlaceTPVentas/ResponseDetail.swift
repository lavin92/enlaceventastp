//
//  ResponseDetail.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 24/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ResponseDetail: NSObject, Mappable {

    var dateAnswer : String?
    var codeAnswer : String?
    var errorCode : String?
    var errorDescription : String?
    var errorMessage : String?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    
    public func mapping(map: Map) {
    
        
        dateAnswer <- map ["FechaRespuesta"]
        codeAnswer <- map ["CodigoRespuesta"]
        errorCode <- map ["CodigoError"]
        errorDescription <- map ["DescripcionError"]
        errorMessage <- map ["MensajeError"]
    
}

}
