//
//  ArrDetalleSolicitud.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 01/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

public class ArrDetalleSolicitud: NSObject, Mappable {
    
    var noContract : String?
    var idOpportunity : String?
    var tsLost : String?
    var dateContract : String?
    var recordTypeId : String?
    var type : String?
    var ownerID : String?
    var tsNegotiation: String?
    var validationTable : String?
    var cellphoneAP__c : String?
    var numberOpportunity : String?
    var employeeId : String?
    var quotationSynchronized : String?
    var ts_start_MC : String?
    var contactBillingEmail : String?
    var statusMC : String?
    var ts_Reactivated : String?
    var billingPotential : String?
    var ts_Rejected : String?
    var amount : String?
    var copiedFiscalPostcard : String?
    var ts_Pending : String?
    var billingSegment : String?
    var motive : String?
    var employeeDistributor : String?
    var colonyFiscal: String?
    var name: String?
    var accountID : String?
    var ts_Assignation : String?
    var cityFiscal : String?
    var contactuBilling : String?
    var tsValidated : String?
    var tsProposal : String?
    
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    
    public func mapping(map: Map) {
    
        noContract <- map["NoContrato"]
        idOpportunity <- map["IdOportunidad"]
        tsLost <- map["TsPerdida"]
        dateContract <- map["FechaContrato"]
        recordTypeId <- map["RecordTypeId"]
        type <- map["Type"]
        ownerID <- map["OwnerID"]
        tsNegotiation <- map["TsNegociacion"]
        validationTable <- map["ValidacionMesa"]
        cellphoneAP__c <- map["CelularAP__c"]
        numberOpportunity <- map["NumeroOportunidad"]
        employeeId <- map["EmpleadoId"]
        quotationSynchronized <- map["CotizacionSincronizada"]
        ts_start_MC <- map["TS_Inicio_MC"]
        contactBillingEmail <- map["ContactoFacturacionEmail"]
        statusMC <- map["EstatusMC"]
        ts_Reactivated <- map["TS_Reactivada"]
        billingPotential <- map["FacturacionPotencial"]
        ts_Rejected <- map["TS_Rechazada"]
        amount <- map["Amount"]
        copiedFiscalPostcard <- map["CopidoPostalFiscal"]
        ts_Pending <- map["TS_Pendiente"]
        billingSegment <- map["SegmentoFacturacion"]
        motive <- map["Motivo"]
        employeeDistributor <- map["EmpleadoDistribuidor"]
        colonyFiscal <- map["ColoniaFiscal"]
        name <- map["Name"]
        accountID <- map["AccountID"]
        ts_Assignation <- map["TS_Asignacion"]
        cityFiscal <- map["CiudadFiscal"]
        contactuBilling <- map["ContactuFacturacion"]
        tsValidated <- map["TsValidada"]
        tsProposal <- map["TsPropuesta"]

}
}
