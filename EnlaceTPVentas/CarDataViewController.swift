//
//  CarDataViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 29/09/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import TextFieldEffects
import CreditCardForm



class CarDataViewController: BaseViewController,CardIOPaymentViewControllerDelegate{

    let mValidator = UtilsTp()

    @IBOutlet weak var numberCardTextField: HoshiTextField!
    @IBOutlet weak var dateExpirationTextField: HoshiTextField!
    
    var mNumberCard : String = ""
    var mMonthExpiration : Int = 0
    var mYearExpiration : Int = 0
    var mMonthExpirationF : String = ""
    var mYearExpirationF : String = ""
    var uno: Bool =  true
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == numberCardTextField{
            return mValidator.numbersAndLength(textField, shouldChangeCharactersIn: range, replacementString: string, maxChar: 16)
        }
        else if textField == dateExpirationTextField{
            return mValidator.numbersAndLength(textField, shouldChangeCharactersIn: range, replacementString: string, maxChar: 4)
        }
        return true
     }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !CardIOUtilities.canReadCardWithCamera() {
        } else {
            CardIOUtilities.preloadCardIO()
        }
        numberCardTextField.delegate = self

        }
    
    override func viewDidAppear(_ animated: Bool){
        if  (uno){
            scanCard()
        }
        else {
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        if let cardInfo = cardInfo {
            mNumberCard = cardInfo.cardNumber
            mYearExpiration = Int(cardInfo.expiryYear)
            mMonthExpiration = Int(cardInfo.expiryMonth)
            mYearExpirationF = String(((cardInfo.expiryYear) % 100))
            mMonthExpirationF = String(format: "%02d", cardInfo.expiryMonth)
            numberCardTextField.text = cardInfo.redactedCardNumber
            dateExpirationTextField.text = "\(mMonthExpirationF)/\(mYearExpirationF) "
            uno = false

        }
        paymentViewController.dismiss(animated: false, completion: nil)
    }
    
    func scanCard() {
        UIBarButtonItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.black], for: UIControlState.normal)
        guard let cardIOViewController = CardIOPaymentViewController(paymentDelegate: self) else {
            return
        }
        cardIOViewController.collectCVV = false
        cardIOViewController.hideCardIOLogo = true
        cardIOViewController.guideColor = UIColor.red
        self.present(cardIOViewController, animated: true) {
            let navigationBarAppearace = UINavigationBar.appearance()
            navigationBarAppearace.isTranslucent = false
            navigationBarAppearace.titleTextAttributes =  [NSForegroundColorAttributeName:UIColor.white]
        }
    }
    
    
    @IBAction func sendControlTableButton(_ sender: Any) {

        let textField1 = mValidator.textFieldDidChange(textField: numberCardTextField!)
        let textField2 = mValidator.textFieldDidChange(textField:dateExpirationTextField!)
        
        if ((textField1)&&( textField2)){
            
        }
        else{
            AlertDialog.show(title: "Error", body: "Favor de Lllenar Todos Los Campos", view: self)
            
       }
    }
}

