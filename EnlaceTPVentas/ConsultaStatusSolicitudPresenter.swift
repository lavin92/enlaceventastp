//
//  ConsultaStatusSolicitudPresenter.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 27/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

public protocol ConsultaStatusSolicitudDelegate : NSObjectProtocol {
    func onSuccessConsultaStatusSolicitud(resultStatus : ResultStatus)
}


class ConsultaStatusSolicitudPresenter: BaseVentasPresenter {
    
    var mConsultaStatusSolicitudDelegate : ConsultaStatusSolicitudDelegate!
    
    init(viewController: BaseViewController, mconsultaStatusSolicitudDelegate: ConsultaStatusSolicitudDelegate) {
        super.init(viewController: viewController)
        mConsultaStatusSolicitudDelegate = mconsultaStatusSolicitudDelegate
    }
    
    
    func consultStatus(ownerID : String, dateStart : String, dateEnd : String){
        let consultaStatusSolicitudRequest : ConsultaStatusSolicitudRequest = ConsultaStatusSolicitudRequest()
        consultaStatusSolicitudRequest.ownerID = ownerID
        consultaStatusSolicitudRequest.DateStart = dateStart
        consultaStatusSolicitudRequest.DateEnd = dateEnd
        RetrofitManager<ConsultaStatusSolicitudResponse>.init(requestUrl: ApiDefinition.WS_CONSULTA_STATUS_SOLICITUD, delegate: self).request(requestModel: consultaStatusSolicitudRequest)
    }
    
    
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        AlertDialog.hideOverlay()
        if requestUrl == ApiDefinition.WS_CONSULTA_STATUS_SOLICITUD{
            let consultaStatusSolicitudResponse : ConsultaStatusSolicitudResponse = response as! ConsultaStatusSolicitudResponse
            if consultaStatusSolicitudResponse.result?.rResult == "0"{
                
mConsultaStatusSolicitudDelegate.onSuccessConsultaStatusSolicitud(resultStatus: consultaStatusSolicitudResponse.resultStatus!)
                
            }else {
                onErrorLoadResponse(requestUrl: requestUrl, messageError: (consultaStatusSolicitudResponse.result?.rDescription)!)
            }
            
            
        }
    }
    
}

