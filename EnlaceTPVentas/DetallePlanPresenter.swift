//
//  DetallePlanPresenter.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 27/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary


public protocol DetallePlanDelegate : NSObjectProtocol {
    func onSuccessDetallePlan()
}



class DetallePlanPresenter: BaseVentasPresenter {
    
    var mDetallePlanDelegate : DetallePlanDelegate!
    
    init(viewController: BaseViewController, mdetallePlanDelegate: DetallePlanDelegate) {
        super.init(viewController: viewController)
        mDetallePlanDelegate = mdetallePlanDelegate
        
        
}
    func loadDetallePlan(idPlan : String){
       let info : Info = Info()
        info.iIdPlan = idPlan
        let detallePlanRequest : DetallePlanRequest = DetallePlanRequest()
        detallePlanRequest.info = Info()
        RetrofitManager<DetallePlanResponse>.init(requestUrl: ApiDefinition.WS_DETALLE_PLAN, delegate: self).request(requestModel: detallePlanRequest)
    }
    
    
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        AlertDialog.hideOverlay()
        if requestUrl == ApiDefinition.WS_DETALLE_PLAN{
            let detallePlanResponse : DetallePlanResponse = response as! DetallePlanResponse
            if detallePlanResponse.result?.rResult == "0"{
                let arrayProductosAdicionales : [ArrProductosAdicionales] = detallePlanResponse.arrProductosAdicionales
                
            }
            
        }
    }

}
