//
//  PaymentAddressBillingViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 04/12/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import TextFieldEffects
import SwiftBaseLibrary

class PaymentAddressBillingViewController: BaseViewController {

    let mValidator = UtilsTp()
    @IBOutlet weak var streetTextField: HoshiTextField!
    @IBOutlet weak var numberInternoTextField: HoshiTextField!
    @IBOutlet weak var numberExternoTextField: HoshiTextField!
    @IBOutlet weak var colonyTextField: HoshiTextField!
    @IBOutlet weak var stateTextField: HoshiTextField!
    @IBOutlet weak var cityTextField: HoshiTextField!
    @IBOutlet weak var delegamuniTextField: HoshiTextField!
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == numberExternoTextField{
            return mValidator.numbersAndLength(textField, shouldChangeCharactersIn: range, replacementString: string, maxChar: 10)
        }
            
        else if  textField == numberInternoTextField {
            return mValidator.numbersAndLength(textField, shouldChangeCharactersIn: range, replacementString: string, maxChar: 10)
        }
        return true
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberExternoTextField.delegate = self
        numberInternoTextField.delegate = self

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
      
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.title = "Método de Pago"
        
    }
    
    
    
    @IBAction func continueButton(_ sender: Any) {
        
        let textField1 = mValidator.textFieldDidChange(textField: streetTextField!)
        let textField2 = mValidator.textFieldDidChange(textField: numberInternoTextField!)
        let textField3 = mValidator.textFieldDidChange(textField: numberExternoTextField!)
        let textField4 = mValidator.textFieldDidChange(textField: colonyTextField!)
        let textField5 = mValidator.textFieldDidChange(textField: stateTextField!)
        let textField6 = mValidator.textFieldDidChange(textField: cityTextField!)
        let textField7 = mValidator.textFieldDidChange(textField:delegamuniTextField!)
        
        
        if ((textField1)&&(textField2)&&(textField3)&&(textField4)&&(textField5)&&(textField6)&&(textField7) ){
            //AlertDialog.show(title: "Correcto", body: "Campos Llenos", view: self)
            
           
            
        }
        else{
            AlertDialog.show(title: "Error", body: "Favor de Llenar Todos los Campos", view: self)
        }
        
    }
    

}
