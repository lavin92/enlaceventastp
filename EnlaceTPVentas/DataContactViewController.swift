//
//  DataContactViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 21/09/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import TextFieldEffects



class DataContactViewController: BaseViewController {
    
    let mValidator = UtilsTp()
    
    @IBOutlet weak var typePersonaIconTextField: IconTextField!
    @IBOutlet weak var typeIdIconTextField: IconTextField!
    @IBOutlet weak var numberIdentificationTextField: HoshiTextField!
    @IBOutlet weak var nameRepreLegalTextField: HoshiTextField!
    @IBOutlet weak var paternalLastNameTextField: HoshiTextField!
    @IBOutlet weak var motherLastNameTextField: HoshiTextField!
    @IBOutlet weak var birthDateIconTextField: IconTextField!
    @IBOutlet weak var rfcTextField: HoshiTextField!
    @IBOutlet weak var phoneTextField: HoshiTextField!
    @IBOutlet weak var phone2TextField: HoshiTextField!
    @IBOutlet weak var cellPhoneNumberTextField: HoshiTextField!
    @IBOutlet weak var emailTextField: HoshiTextField!
    @IBOutlet weak var contractNumberTextField: HoshiTextField!
    @IBOutlet weak var mViewPersonMoral: UIView!
    @IBOutlet weak var nameCompanyTextField: HoshiTextField!
    @IBOutlet weak var reasonSocialTextField: HoshiTextField!
    @IBOutlet weak var typeCoinTextField: HoshiTextField!
    @IBOutlet weak var billingPotentialIconTextField: IconTextField!
    @IBOutlet weak var billingSegmentTextField: HoshiTextField!
    @IBOutlet weak var callCenterButton: CheckBoxButton!
    @IBOutlet weak var distributionButton: CheckBoxButton!
    @IBOutlet weak var executiveSalesButton: CheckBoxButton!
    @IBOutlet weak var strategicButton: CheckBoxButton!
    
    
    
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTextField{
            return mValidator.numbersAndLength(textField, shouldChangeCharactersIn: range, replacementString: string, maxChar: 10)
        }
        if textField == phone2TextField{
            return mValidator.numbersAndLength(textField, shouldChangeCharactersIn: range, replacementString: string, maxChar: 10)
        
        }
        if  textField == cellPhoneNumberTextField{
            return mValidator.numbersAndLength(textField, shouldChangeCharactersIn: range, replacementString: string, maxChar: 10)
        }
            if  textField == contractNumberTextField {
                return mValidator.numbersAndLength(textField, shouldChangeCharactersIn: range, replacementString: string, maxChar: 10)
            
        }else if textField == numberIdentificationTextField{
            return mValidator.numbersAndLength(textField, shouldChangeCharactersIn: range, replacementString: string, maxChar: 10)
        }
        return true
    }
    
    
    var mTypePerson: [String] = ["Fisica con actividad empresarial","Moral"]
    var mIdentificationType: [String] = ["IFE/INE","Licencia","Pasaporte","Cédula Profecional","FMM","INAPAM"]
    var mFacturationPotential : [String] = ["De $1,000 a $30,000","De $30,001 a $100,000","De $1000,001 a $500,000","Mas de $500,001","Mas de $1,000,000"]
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        ViewUtils.setVisibility(view: mViewPersonMoral, visibility: .GONE)
        
      
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     self.navigationItem.title = "Datos del Contacto"
        
     phoneTextField.delegate = self
     phone2TextField.delegate = self
     cellPhoneNumberTextField.delegate = self
     numberIdentificationTextField.delegate = self
     contractNumberTextField.delegate = self 
        
     typeCoinTextField.text = "PESO"
     typePersonaIconTextField.setTextPicker(elements:mTypePerson)
     typeIdIconTextField.setTextPicker(elements:mIdentificationType)
     billingPotentialIconTextField.setTextPicker(elements:mFacturationPotential)
     birthDateIconTextField.setDatePicker()
        
    }
    
    @IBAction func typePerson(_ sender: Any) {
        ViewUtils.setVisibility(view: mViewPersonMoral, visibility: .GONE)
        
        if (typePersonaIconTextField.text == "Moral"){
            ViewUtils.setVisibility(view: mViewPersonMoral, visibility: .NOT_GONE)
        }else {
            ViewUtils.setVisibility(view: mViewPersonMoral, visibility: .GONE)
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    @IBAction func continueButton(_ sender: Any) {
        
        let providedEmailAddress = emailTextField.text
        
        let isEmailAddressValid = isValidEmailAddress(emailAddressString: providedEmailAddress!)
        
        if isEmailAddressValid{
            print("El Correo Electrónico no es valido ")
        }else{
            print(" ")
            displayAlertMessage(messageToDisplay: "El Correo Electrónico no es valido ")
        }
        
        let textField1 = mValidator.textFieldDidChange(textField: numberIdentificationTextField!)
        let textField2 = mValidator.textFieldDidChange(textField: nameRepreLegalTextField)
        let textField3 = mValidator.textFieldDidChange(textField: paternalLastNameTextField!)
        let textField4 = mValidator.textFieldDidChange(textField: motherLastNameTextField!)
        let textField5 = mValidator.textFieldDidChange(textField: rfcTextField!)
        let textField6 = mValidator.textFieldDidChange(textField: phoneTextField!)
        let textField7 = mValidator.textFieldDidChange(textField: phone2TextField!)
        let textField8 = mValidator.textFieldDidChange(textField: cellPhoneNumberTextField!)
        let textField9 = mValidator.textFieldDidChange(textField: contractNumberTextField!)
        let textField10 = mValidator.textFieldDidChange(textField: typeCoinTextField!)
        let textField11 = mValidator.textFieldDidChange(textField: billingSegmentTextField!)
        
        if ((textField1)&&(textField2)&&(textField3)&&(textField4)&&(textField5)&&(textField6)&&(textField7)&&(textField8)&&(textField9)&&(textField10)&&(textField11)){
            //AlertDialog.show(title: "Correcto", body: "Campos Llenos", view: self)
            ViewControllerUtils.pushViewController(from: self , to: SelectCityViewController.self)
        }
        else{
            AlertDialog.show(title: "Error", body: "Favor de Llenar Todos los Campos", view: self)
        }
    
        
        }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    func displayAlertMessage(messageToDisplay:String){
    
    let alertController = UIAlertController(title: "Alert title", message: messageToDisplay, preferredStyle: .alert)
    
    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
        
        // Code in this block will trigger when OK button tapped.
        print("Ok button tapped");
        
    }
    
    alertController.addAction(OKAction)
    
    self.present(alertController, animated: true, completion:nil)
    
    }
}
    
    
    
    
    


