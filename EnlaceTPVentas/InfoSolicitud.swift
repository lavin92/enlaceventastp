//
//  InfoSolicitud.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 14/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class InfoSolicitud: NSObject, Mappable{

    var iIDUser : String?
    var iDateStart : String?
    var iDateEnd : String?
    
    required convenience init?(map: Map){
        self.init()
        
    }
    
    public func mapping(map: Map) {
       
        iIDUser <- map["IDUser"]
        iDateStart <- map["FechaInicio"]
        iDateEnd <- map["FechaFin"]
        
    }
}

