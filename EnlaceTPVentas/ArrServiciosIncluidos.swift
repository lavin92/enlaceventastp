//
//  ArrServiciosIncluidos.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 31/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ArrServiciosIncluidos: NSObject, Mappable {
   
    var id : String?
    var name : String?
    var editableName : String?
    var planId : String?
    var price : String?
    var isActivated : String?
    var isInvoiced : String?
    var serviceId : String?
    var modeSRV : String?
    var typeService : String?
    var typeTelephony : String?
    var arrProductosIncluidos: [ArrProductosIncluidos] = []
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    public func mapping(map: Map) {
    
        id <- map["Id"]
        name <- map["Nombre"]
        editableName <- map["NombreEditable"]
        planId <- map["PlanId"]
        price <- map["Precio"]
        isActivated <- map["SeActiva"]
        isInvoiced <- map["SeFactura"]
        serviceId <- map["ServicioId"]
        modeSRV <- map["SRV_Mode"]
        typeService <- map["TipoServicio"]
        typeTelephony <- map["TipoTelefonia"]
        arrProductosIncluidos <- map["ArrProductosIncluidos"]
    }

}
