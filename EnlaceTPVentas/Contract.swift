//
//  Contract.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 20/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class Contract: NSObject, Mappable {

    var dateContract : String?
    var rfc : String?
    var identificationNumber : String?
    var name : String?
    var lastnameFather : String?
    var lastnameMother : String?
    var phone : String?
    var cellPhone : String?
    var email : String?
    var delegationMunicipality : String?
    var city : String?
    var zipCode : String?
    var state: String?
    var street : String?
    var numberExterno : String?
    var numberInterno : String?
    var colony : String?
    
    //other data
    
    var typeContract : String?
    var reasonSocial: String?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        
        dateContract <- map["FechaContrato"]
        rfc <- map["RFC"]
        identificationNumber <- map["NumeroIdentificacion"]
        name <- map["Nombre"]
        lastnameFather <- map["ApellidoPaterno"]
        lastnameMother <- map["ApellidoMaterno"]
        phone <- map["Telefon"]
        cellPhone <- map["Celular"]
        email <- map["Email"]
        delegationMunicipality <- map["DelegacionMunicipio"]
        city <- map["Ciudad"]
        zipCode <- map["CodigoPostal"]
        state <- map["Estado"]
        street <- map["Calle"]
        numberExterno <- map["NumeroExterior"]
        numberInterno <- map["NumeroInterior"]
        colony <- map["Colonia"]
        
        //other data
        
        typeContract <- map["TipoContrato"]
        reasonSocial <- map["RazonSocial"]
        
    
}

}

