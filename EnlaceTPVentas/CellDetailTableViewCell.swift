//
//  CellDetailTableViewCell.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 26/09/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit

class CellDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var imageCellImageView: UIImageView!
    @IBOutlet weak var tittleCellLabel: UILabel!
    @IBOutlet weak var tittle2CellLabel: UILabel!
    @IBOutlet weak var tittle3CellLabel: UILabel!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
