//
//  WelcomeViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 25/09/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class WelcomeViewController: BaseViewController,iCarouselDataSource, iCarouselDelegate{
    
    
    
    @IBOutlet weak var mCarousel: iCarousel!
    
    @IBOutlet weak var nameEmployeeLabel: UILabel!
    
    
    
    var arrayItems = [Card]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameEmployeeLabel.text = extras ["name"] as! String
        
        /*let obj: Card = Card()
         obj.image = "img_mis_Ventas"
         arrayItems.append(obj)*/
        
        
        let obj1: Card = Card()
        obj1.image = "img_Lanzamiento"
        arrayItems.append(obj1)
        
        
        let obj2: Card = Card()
        obj2.image = "img_oferta_comercial"
        arrayItems.append(obj2)
        
        
        mCarousel.type = .coverFlow2
        mCarousel.delegate = self
        mCarousel.dataSource = self
        mCarousel.reloadData()
        // Do any additional setup 2after loading the view.
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: icarousel delegate methods
    
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        return arrayItems.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        let view = Bundle.main.loadNibNamed("ItemCellCollectionReusableView", owner: self, options: nil)![0] as! ItemCellCollectionReusableView
        /*view.frame.size = CGSize(width: self.view.frame.size.width/2, height: 83.0)
         view.backgroundColor = UIColor.lightGray*/
        
        view.imageCellImageView.image = UIImage(named:arrayItems[index].image!)
        return view
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        print("\(index)")
        /*if (index == 0 ) {
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "MySalesViewController")
         as! MySalesViewController
         navigationController?.pushViewController(vc,animated: true)
         
         }*/
        
        if(index == 0){
            ViewControllerUtils.pushViewController(from: self, to: LaunchPromotionsViewController.self)
            
        }else if(index == 1){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "OfertaComercialViewController")
                as! OfertaComercialViewController
            navigationController?.pushViewController(vc,animated: true)
            
        }
        
        //self .performSegue(withIdentifier: "imageDisplaySegue", sender: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        
    }
    
    
    
    
    @IBAction func logoutButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func startSaleButton(_ sender: Any) {
        ViewControllerUtils.pushViewController(from: self, to: MapViewController.self)
    }
    
    
    
    
    
}





