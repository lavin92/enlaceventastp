//
//  RestFaultElement.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 01/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class RestFaultElement: NSObject, Mappable {
   
    var sumry: String?
    var cod : String?
    var dtail: String?
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    public func mapping(map: Map) {
        
        sumry <- map["summary"]
        cod <- map["code"]
        dtail <- map["detail"]
}
}
