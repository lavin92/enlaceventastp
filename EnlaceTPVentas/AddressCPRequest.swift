//
//  AddressCPRequest.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 13/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class AddressCPRequest: BaseRequest {
    
    var cp : String?
    
    override init() {
        super.init()
    }
    
    init(cp : String) {
        super.init()
        self.cp = cp
    }
    
    public required init?(map: Map) {
        fatalError("init(map:) has not been implemented")
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        cp <- map["CodigoPostal"]
    }
    
}
