//
//  ArrayPromotions.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 13/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ArrayPromotions: NSObject, Mappable {
    
    var id : String?
    var name : String?
    var commentary: String?
    var status : String?
    var productId : String?
    var productExcIncId : String?
    var type : String?
    
    
    
    
    required convenience init?(map: Map) {
        self.init()
   
    }
    
    
    public func mapping(map: Map) {

        id <- map["Id"]
        name <- map["Name"]
        commentary <- map["Comentario"]
        status <- map["Estatus"]
        productId <- map["ProductoId"]
        productExcIncId <- map["ProductoExcIncId"]
        type <- map["Tipo"]
        

    }

}

