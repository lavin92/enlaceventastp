//
//  ArrSitioPlan.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 30/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ArrSitioPlan: NSObject, Mappable {
    
    var dp_Plan : String = ""
    var namePlan : String = ""
    var arrCotPlanPromotion: [ArrCotPlanPromotion] = []
    var arrCotPlanServicioPrimero: [ArrCotPlanServicioPrimero] = []
    var arrCotPlanServicioSegundo: [ArrCotPlanServicioSegundo] = []
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    public func mapping(map: Map) {
        
        dp_Plan <- map ["DP_Plan"]
        namePlan <- map ["NombrePlan"]
        arrCotPlanPromotion <- map ["Cot_PlanPromocion"]
        arrCotPlanServicioPrimero <- map ["Cot_PlanServicioPrimero"]
        arrCotPlanServicioSegundo <- map ["Cot_PlanServicioSegundo"]
        
        
        
    }
}

