//
//  ConsultaPlanesResponse.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 30/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class ConsultaPlanesResponse: BaseResponse  {
    
    var result : Result?
    var arrayDetallePlanes: [ArrayDetallePlanes] = []
    
    public required convenience init?(map: Map) {
        self.init()
        
    }
    
    
    public override func mapping (map: Map) {
        
        result <- map["Result"]
        arrayDetallePlanes <- map["ArrayDetallePlanes"]
    }
    
}
