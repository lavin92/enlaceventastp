//
//  CellSalesStatusTableViewCell.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 02/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit

class CellSalesStatusTableViewCell: UITableViewCell {

    @IBOutlet weak var tittleCellLabel: UILabel!
    @IBOutlet weak var imageCellImageView: UIImageView!
    @IBOutlet weak var tittle2CellLabel: UILabel!
    @IBOutlet weak var tittle3CellLabel: UILabel!
    @IBOutlet weak var tittle4CellLabel: UILabel!
    @IBOutlet weak var tittle5CellLabel: UILabel!
    @IBOutlet weak var tittle6CellLabel: UILabel!
    @IBOutlet weak var tittle7CellLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

