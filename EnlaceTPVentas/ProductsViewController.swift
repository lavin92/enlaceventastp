//
//  ProductsViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 27/09/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class ProductsViewController: BaseViewController {
    
   
    @IBOutlet weak var productsTableView: UITableView!
    
    
    var arrayItems = [List]()
    var listDataSource : ListDataSources!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Cotización"
        
        let obj: List = List()
        obj.tittle = "WiFi extender"
        //obj.image = "photo_01"
        arrayItems.append(obj)
        
        let obj1: List = List()
        obj1.tittle = "Antivirus 10 dispositivos"
        arrayItems.append(obj1)
        
        let obj2: List = List()
        obj2.tittle = "Asistencia online 10 dispositivos"
        arrayItems.append(obj2)
        
        let obj3: List = List()
        obj3.tittle = "Backup 50 GB"
        arrayItems.append(obj3)
        
        let obj4: List = List()
        obj4.tittle = "Antivirus 20 dispositivos"
        arrayItems.append(obj4)
        
        let obj5: List = List ()
        obj5.tittle = "Backup 1 TB"
        arrayItems.append(obj5)
        
        let obj6: List = List ()
        obj6.tittle = "Asistencia online 20 dispositivos"
        arrayItems.append(obj6)
        
        
        
        
        let nib = UINib(nibName: "CellAdditionalSettingsTableViewCell", bundle: nil)
        productsTableView.register(nib, forCellReuseIdentifier: "CellAdditionalSettingsTableViewCell")
        listDataSource = ListDataSources(tableView: self.productsTableView)
        listDataSource?.update(coleccionInfo: arrayItems)
        self.productsTableView.reloadData()
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func continueButton(_ sender: Any) {
        ViewControllerUtils.pushViewController(from: self, to: QuotationPackageViewController.self)
        
    }
    
}

