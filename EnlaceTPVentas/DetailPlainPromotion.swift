//
//  DetailPlainPromotion.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 16/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper



class DetailPlainPromotion: NSObject, Mappable {

    dynamic var uuid : String = UUID().uuidString
    dynamic var id : String?
   
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
     public func mapping(map: Map) {
    
        id <- map["Id"]
    
    }
    
 
}
