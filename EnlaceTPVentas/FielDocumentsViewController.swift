//
//  FielDocumentsViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 03/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class FielDocumentsViewController: BaseViewController {
    
    @IBOutlet weak var mFrontPhoto: UIImageView!
    @IBOutlet weak var mBackPhoto: UIImageView!
    @IBOutlet weak var mOtherId: UIImageView!
    //@IBOutlet weak var firmElectronicView: UIView!
    @IBOutlet weak var mViewIdentificationFiel: UIView!
    @IBOutlet weak var useIdentificationButton2: CheckBoxButton!
    //@IBOutlet weak var captureFirmImageView: UIImageView!
    //@IBOutlet weak var captureFirmaView: UIView!
   
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       /* let myColor = UIColor.black
        captureFirmaView.layer.borderColor = myColor.cgColor
        captureFirmaView.layer.borderWidth = 1.0*/
        
        
       
    }
    
    @IBAction func useIdentificationButton2(_ sender: Any) {
    
        if (useIdentificationButton2.isChecked){
            ViewUtils.setVisibility(view: mViewIdentificationFiel, visibility: .GONE)
        }else{
            
            ViewUtils.setVisibility(view: mViewIdentificationFiel, visibility: .NOT_GONE)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func frontCameraButton(_ sender: Any) {
    }
    
    @IBAction func reverseCameraButton(_ sender: Any) {
    }
    
    @IBAction func otherIdentificationCameraButton(_ sender: Any) {
    }
    
   
    @IBAction func continueButton(_ sender: Any) {
        ViewControllerUtils.pushViewController(from: self, to: PaymentCardInformationViewController.self)
        
    }
    
    /*@IBAction func firmaButton(_ sender: Any) {
        ViewControllerUtils.presentViewControllerWithResult(from: self, to: FirmElectronicViewController.self, request: "")

    }*/
    
    /*func viewControllerForResult(keyRequest:String,result:ViewControllerResult,data:[String:AnyObject]){
        
        if (result == .RESULT_OK){
            if(data["Signature"] != nil){
                let img: UIImage = data ["Signature"] as! UIImage
                captureFirmImageView.image = img
            }
        }
    }*/
    
}


