//
//  DetalleSolicitudPresenter.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 27/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary



public protocol DetalleSolicitudDelegate : NSObjectProtocol {
    func onSuccessDetalleSolicitud(arrDetalleSolicitud : [ArrDetalleSolicitud])
}


class DetalleSolicitudPresenter: BaseVentasPresenter{
    
    var mDetalleSolicitudDelegate : DetalleSolicitudDelegate!
    
    init(viewController: BaseViewController, mdetalleSolicitudDelegate: DetalleSolicitudDelegate) {
        super.init(viewController: viewController)
        mDetalleSolicitudDelegate = mdetalleSolicitudDelegate
        
        
    }
    
    
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        AlertDialog.hideOverlay()
        if requestUrl == ApiDefinition.WS_DETALLE_SOLICITUD{
            let detalleSolicitudResponse : DetalleSolicitudResponse = response as! DetalleSolicitudResponse
            if (detalleSolicitudResponse.result != nil) {
                
                mDetalleSolicitudDelegate.onSuccessDetalleSolicitud(arrDetalleSolicitud: detalleSolicitudResponse.arrDetalleSolicitud)
                
                
            }else {
                
                onErrorLoadResponse(requestUrl: requestUrl, messageError: (detalleSolicitudResponse.result?.rDescription)!)
                
                
                
                
            }
        }
        
    }
    
}

