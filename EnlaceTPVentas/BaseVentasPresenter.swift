//
//  BaseVentasPresenter.swift
//  EnlaceTPVentas
//
//  Created by Jorge Hdez VIlla on 22/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class BaseVentasPresenter: BasePresenter, AlamofireResponseDelegate {
    
    var mDataManager : DataManager!
    
    override init(viewController: BaseViewController) {
        super.init(viewController: viewController)
        mDataManager = DataManager().getInstance()
    }
    
    func onRequestWs(){
        AlertDialog.showOverlay()
    }
    
    func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        
    }
    
    func onErrorLoadResponse(requestUrl : String, messageError : String){
        AlertDialog.hideOverlay()
        if messageError == "" {
            AlertDialog.show( title: "Error", body: StringDialogs.dialog_error_intern, view: mViewController)
        } else {
            AlertDialog.show(title: "Ha ocurrido un error", body: messageError, view: mViewController)
        }
    }
    
    func onErrorConnection(){
        AlertDialog.hideOverlay()
        AlertDialog.show( title: "Error", body: StringDialogs.dialog_error_connection, view: mViewController)
    }
    
    
}

