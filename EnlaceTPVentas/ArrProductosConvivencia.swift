//
//  ArrProductosConvivencia.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 01/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ArrProductosConvivencia: NSObject, Mappable {

    var id : String?
    var name : String?
    var commentary : String?
    var status : String?
    var productId : String?
    var productoExcIncId : String?
    var type : String?
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    public func mapping(map: Map) {
    
        id <- map["Id"]
        name <- map["Name"]
        commentary <- map["Comentario"]
        status <- map["Estatus"]
        productId <- map["ProductoId"]
        productoExcIncId <- map["ProductoExcIncId"]
        type <- map["Tipo"]
}
}
