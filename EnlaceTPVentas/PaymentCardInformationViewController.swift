//
//  PaymentCardInformationViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 04/12/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class PaymentCardInformationViewController: BaseViewController{
    @IBOutlet weak var sameCheck: UIImageView!
    @IBOutlet weak var diferenChek: UIImageView!
    @IBOutlet weak var sameDirectionCheck: UIImageView!
    @IBOutlet weak var diferentDirectionCheck: UIImageView!
    
    var radioClic1 = true
    var radioClic2 = false
    var radioClic3 = true
    var radioClic4 = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    self.navigationItem.title = "Método de Pago"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func sameHolder(_ sender: Any) {
        radioClic1 = true
        radioClic2 = false
        headline(radio1: radioClic1, radio2: radioClic2,botonA: sameCheck,botonB:diferenChek)
    }
    @IBAction func diferentHolder(_ sender: Any) {
        radioClic1 = false
        radioClic2 = true
       headline(radio1: radioClic1, radio2: radioClic2,botonA: sameCheck,botonB:diferenChek)
    }
    @IBAction func sameBilling(_ sender: Any) {
        radioClic3 = true
        radioClic4 = false
        headline(radio1: radioClic3, radio2: radioClic4,botonA: sameDirectionCheck,botonB:diferentDirectionCheck)
    }
    @IBAction func diferentBilling(_ sender: Any) {
        radioClic3 = false
        radioClic4 = true
        headline(radio1: radioClic3, radio2: radioClic4,botonA: sameDirectionCheck,botonB:diferentDirectionCheck)
    }
    func headline(radio1: Bool,radio2: Bool,botonA: UIImageView,botonB: UIImageView){
        if  radio1 {
            botonA.image = UIImage(named: "icon_check_on.png")
            botonB.image = UIImage(named: "icon_check_off.png")
        }
        else {
            botonA.image = UIImage(named: "icon_check_off.png")
            botonB.image = UIImage(named: "icon_check_on.png")
        }
    }
    @IBAction func nextPage(_ sender: UIButton) {
        print(radioClic1)
        print(radioClic2)
        print(radioClic3)
        print(radioClic4)
     
        if (radioClic1 && !radioClic2 && radioClic3 && !radioClic4){
             ViewControllerUtils.pushViewController(from: self, to: CarDataViewController.self)
            
        }
        else {
            AlertDialog.show(title: "mensaje", body: "tipo 2", view: self)
        }

   
        
    }
    
}
