//
//  MySalesViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 05/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import UICircularProgressRing

class MySalesViewController: UIViewController, UICircularProgressRingDelegate {
    
    
  
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.title = "Mis ventas"
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        }
    
    
        
        
    
    @IBAction func animateTheViews(_ sender: Any) {
        
        
    
    }
    
    
    @IBAction func approvedButton1(_ sender: Any) {
        
      let storyboard = UIStoryboard(name: "Main", bundle: nil)
      let vc = storyboard.instantiateViewController(withIdentifier: "ApprovedSalesViewController")
          as! ApprovedSalesViewController
      navigationController?.pushViewController(vc,animated: true)
    }
    
    @IBAction func pendingButton1(_ sender: Any) {
        
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
let vc = storyboard.instantiateViewController(withIdentifier:"PendingSalesViewController")
        as! PendingSalesViewController
       navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func rejectedButton1(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RejectedSalesViewController")
        as! RejectedSalesViewController
       navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func approvedButton2(_ sender: Any) {

       let storyboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ApprovedSales2ViewController")
       as! ApprovedSales2ViewController
       navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func pendingButton2(_ sender: Any) {

let storyboard = UIStoryboard(name: "Main", bundle:nil)
let vc = storyboard.instantiateViewController(withIdentifier:
    "PendingSales2ViewController")
as! PendingSales2ViewController
      navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func rejectedButton2(_ sender: Any) {
        
   let storyboard = UIStoryboard(name: "Main", bundle:nil)
    let vc = storyboard.instantiateViewController(withIdentifier:"RejectedSales2ViewController")
        as! RejectedSales2ViewController
       navigationController?.pushViewController(vc, animated: true)
    }
}

