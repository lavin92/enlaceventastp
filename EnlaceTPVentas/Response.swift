//
//  Response.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 13/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class Response: NSObject, Mappable {

    var rResult : String?
    var rResultId : String?
    var rDescription : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        rResult <- map["Result"]
        rResultId <- map["ResultId"]
        rDescription <- map["Description"]
}

}
