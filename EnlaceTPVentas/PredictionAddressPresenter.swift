//
//  PredictionAddressPresenter.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 12/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//
import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import SwiftBaseLibrary

class PredictionAddressPresenter : BaseVentasPresenter {
    
    var mPredictionDelegate : PredictionAddressDelegate?
    var request : Alamofire.Request?
    
    private let baseURLString = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    private let baseDetailtsURLString = "https://maps.googleapis.com/maps/api/place/details/json"
    
    init(delegate : PredictionAddressDelegate) {
        super.init(viewController: BaseViewController())
        self.mPredictionDelegate = delegate
    }
    
    func getPredictionAddress(keyword: NSString){
        if ConnectionUtils.isConnectedToNetwork(){
            let escapedString = keyword.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            let urlString = "\(baseURLString)?key=\(Strings.GOOGLE_MAP_KEY)&language=es&input=\(String(describing: escapedString!))"
            
            request = Alamofire.request(urlString, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default,headers: nil).responseObject {
                
                (response: DataResponse<DirectionResponse>) in
                
                switch response.result {
                    
                case .success:
                    
                    let objectReponse : DirectionResponse = response.result.value!
                    if(!objectReponse.predictionData.isEmpty){
                        self.mPredictionDelegate?.onSuccessPrediction(collectionAddress: objectReponse.predictionData)
                    }else{
                        self.onErrorLoadResponse(requestUrl: urlString, messageError: "ocurrio un error al consultar google maps")
                    }
                case .failure(_):
                   self.onErrorLoadResponse(requestUrl: urlString, messageError: "ocurrio un error al consultar la BD")
                }
            }
            
        } else {
            self.onErrorConnection()
        }
    }
    
    func getLatLon(placeId : String){
        if ConnectionUtils.isConnectedToNetwork(){
            
            let urlString = "\(baseDetailtsURLString)?placeid=\(placeId)&key=\(Strings.GOOGLE_MAP_KEY)"
            
            request = Alamofire.request(urlString, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default,headers: nil).responseObject {
                
                (response: DataResponse<GeometryResponse>) in
                
                switch response.result {
                    
                case .success:
                    
                    let objectReponse : GeometryResponse = response.result.value!
                    if((objectReponse.result?.geometry?.location?.lat != nil)){
                        self.mPredictionDelegate?.onSuccessGeometry(geoLatLon: (objectReponse.result?.geometry!)!, formatAddress: (objectReponse.result?.formattedAddress)!)
                    }else{
                        self.onErrorLoadResponse(requestUrl: urlString, messageError: "Ocurrio un error al consultar google maps")
                    }
                case .failure(_):
                    self.onErrorLoadResponse(requestUrl: urlString, messageError:"Ocurrio un error al consultar la BD")
                }
            }
            
        } else {
            self.onErrorConnection()
        }
    }
    
}

