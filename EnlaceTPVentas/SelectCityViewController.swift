//
//  SelectCityViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 04/12/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class SelectCityViewController: BaseViewController {
    
    
    @IBOutlet weak var selectCityIconTextField: IconTextField!
    
    
    var mSelectCity: [String] = ["AGUASCALIENTES","CACUN","CELAYA","CHIHUAHUA","CUERNAVACA","GUADALAJARA","CIUDAD JUAREZ","LEON","MERIDA","CIUDAD DE MÉXICO","MONTERREY","MORELIA","PACHUCA","PUEBLA","QUERETARO","SAN LUIS POTOSI","TIJUANA","TOLUCA","XALAPA","VERACRUZ","HERMOSILLO","TUXTLA GUTIERREZ"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectCityIconTextField.setTextPicker(elements: mSelectCity)
        self.navigationItem.title = "Configurar Paquete"
        
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.title =  "Configurar Paquete"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    @IBAction func continueButton(_ sender: Any) {
        ViewControllerUtils.pushViewController(from: self, to: SelectPlazoViewController.self)
        
    }
    
    
}

    

