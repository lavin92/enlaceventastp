//
//  Suburb.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 13/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ArrColony: NSObject, Mappable {
    
    dynamic var id : String?
    dynamic var name : String?
    dynamic var suburb : String?
    dynamic var city : String?
    dynamic var key : String?
    dynamic var cluster : String?
    dynamic var delegation : String?
    dynamic var state : String?
    dynamic var stateEnId : String?
    dynamic var stateTpId : String?
    dynamic var delegationEnId : String?
    dynamic var delegationTpId : String?
    dynamic var idSource : String?
    dynamic var idPopulationEn : String?
    dynamic var idPopulationTP : String?
    dynamic var idRegionEn : String?
    dynamic var idRegionTP : String?
    dynamic var limit : String?
    dynamic var limit1 : String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        self.id                <- map["Id"]
        self.name                <- map["Nombre"]
        self.suburb               <- map["Colonia"]
        self.city                <- map["Ciudad"]
        self.key                <- map["Clave"]
        self.cluster                <- map["Cluster"]
        self.delegation                <- map["DelegacionMunicipio"]
        self.state                <- map["Estado"]
        self.stateEnId                <- map["IdEstadoEN"]
        self.stateTpId                <- map["IdEstadoTP"]
        self.delegationEnId                <- map["IdMunicipioEN"]
        self.delegationTpId                <- map["IdMunicipioTP"]
        self.idSource                <- map["idOrigen"]
        self.idPopulationEn                <- map["IdProblacionEN"]
        self.idPopulationTP                <- map["IdProblacionTP"]
        self.idRegionEn                <- map["IdRegionEN"]
        self.idRegionTP                <- map["IdRegionTP"]
        self.limit                <- map["Limite"]
        self.limit1                <- map["Limite1"]
    }
    
}
