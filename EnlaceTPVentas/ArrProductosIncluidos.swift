//
//  ArrProductosIncluidos.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 31/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ArrProductosIncluidos: NSObject, Mappable {

    var id : String?
    var name : String?
    var itsAutomaticCity : String?
    var itsOnlyCharge : String?
    var status : String?
    var istVisibility : String?
    var idBrmArrear : String?
    var idBrmCU : String?
    var idBrmForward : String?
    var ieps : String?
    var iva : String?
    var maximumAdd : String?
    var nameEditable : String?
    var planDiscountId : String?
    var plazo : String?
    var basePrice : String?
    var priceEditable : String?
    var priceSoonPayment : String?
    var typeProduct : String?
    var idProduct : String?
    var nameProduct : String?
    var productId : String?
    var hasIpDynamic : String?
    var hasIpFixed : String?
    var hasSTBAAditional : String?
    var isCCTV : String?
    var isWiFi : String?
    var quantity : String?
    var statusProduct : String?
    var isSoonPayment : String?
    var order : String?
    //var productFather : String?
    
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    public func mapping(map: Map) {
    
        id <- map["Id"]
        name <- map["Nombre"]
        itsAutomaticCity <- map["EsAutomaticoCiudad"]
        itsOnlyCharge <- map["EsCargoUnico"]
        status <- map["Estatus"]
        istVisibility <- map["EsVisible"]
        idBrmArrear <- map["IdBrmArrear"]
        idBrmCU <- map["IdBrmCU"]
        idBrmForward <- map["IdBrmForward"]
        ieps <- map["IEPS"]
        iva <- map["IVA"]
        maximumAdd <- map["MaximoAgregar"]
        nameEditable <- map["NombreEditable"]
        planDiscountId <- map["PlanDescuentoId"]
        plazo <- map["Plazo"]
        basePrice <- map["PrecioBase"]
        priceEditable <- map["PrecioEditable"]
        priceSoonPayment <- map["PrecioProntoPago"]
        typeProduct <- map["TipoProducto"]
        idProduct <- map["IdProducto"]
        nameProduct <- map["NameProducto"]
        productId <- map["ProductoId"]
        hasIpDynamic <- map["TieneIPDinamica"]
        hasIpFixed <- map["TieneIPFija"]
        hasSTBAAditional <- map["TieneSTBAdicional"]
        isCCTV <- map["EsCCTV"]
        isWiFi <- map["EsWiFi"]
        quantity <- map["Cantidad"]
        status <- map["EstatusProducto"]
        isSoonPayment <- map["EsProntoPago"]
        order <- map["Orden"]
        //productFather <- map["ProductoPadre"]
    
}
}
