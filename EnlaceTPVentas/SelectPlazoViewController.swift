//
//  SelectPlazoViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 04/12/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class SelectPlazoViewController: BaseViewController {

    
    @IBOutlet weak var zeroMonthsButton: CheckBoxButton!
    @IBOutlet weak var twentyFourMonthsButton: CheckBoxButton!
    @IBOutlet weak var thirtySixMonthsButton: CheckBoxButton!
    @IBOutlet weak var fortyEightMonthsButton: CheckBoxButton!
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.navigationItem.title = "Configurar Paquete"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func continueButton(_ sender: Any) {
        ViewControllerUtils.pushViewController(from: self , to: PageViewController.self)
       
        
    }
    
    
    
    
}

