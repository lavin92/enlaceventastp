//
//  Prospectus.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 30/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class Prospectus: NSObject, Mappable {

    var salesman : Salesman?
    var opportunity : Opportunity?
    var paymentMethod : PaymentMethod?
    var contract : Contract?
    var arrSitios : [ArrSitios] = []
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    public func mapping(map: Map) {
        
        salesman <- map ["Vendedor"]
        opportunity <- map ["Oportunidad"]
        paymentMethod <- map ["MetodoPago"]
        contract <- map ["Contrato"]
        arrSitios <- map ["Sitios"]
        
    
}
}
