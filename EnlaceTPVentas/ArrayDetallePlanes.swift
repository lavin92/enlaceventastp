//
//  ArrayDetallePlanes.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 30/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ArrayDetallePlanes: NSObject, Mappable {

    var arrPlanDetail : String?
    var arrPlanURLSlsfPromo : String?
    var arrPlanURLSlsf : String?
    var arrId : String?
    var arrCity : String?
    var arrPlanID : String?
    var arrName : String?
    var arrPlanPlanzo : String?
    var arrPlanTypeCoverage : String?
    var arrTypePlan : String?
    var arrPlanFamilyId : String?
    var arrPlanFamilyName : String?
    var arrPlanFamilyType : String?
    var arrPlanImage : String?
    var arrPriceList : String?
    var arrPricePaySoon : String?
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    public func mapping(map: Map) {
    
    arrPlanDetail <- map["PlanDetalle"]
    arrPlanURLSlsfPromo <- map["PlanURLSlsfPromo"]
    arrPlanURLSlsf <- map["PlanURLSlsf"]
    arrId <- map["Id"]
    arrCity <- map["Ciudad"]
    arrPlanID <- map["PlanID"]
    arrName <- map["Name"]
    arrPlanPlanzo <- map["PlanPlanzo"]
    arrPlanTypeCoverage <- map["PlanTipoCobertura"]
    arrTypePlan <- map["TipoPlan"]
    arrPlanFamilyId <- map["PlanFamiliaId"]
    arrPlanFamilyName <- map["PlanFamiliaName"]
    arrPlanFamilyType <- map["PlanFamiliaTipo"]
    arrPlanImage <- map["PlanImagen"]
    arrPriceList <- map["PrecioLista"]
    arrPricePaySoon <- map["PrecioProntoPago"]
        
    }
    
}
