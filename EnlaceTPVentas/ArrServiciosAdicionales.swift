//
//  ArrServiciosAdicionales.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 01/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ArrServiciosAdicionales: NSObject, Mappable {

    var id : String?
    var name : String?
    var maximumAdd : String?
    var nameCommercial : String?
    var nameEditable : String?
    var planId : String?
    var price : String?
    var isActivated : String?
    var isInvoice : String?
    var serviceId : String?
    var srv_Mode : String?
    var typeService : String?
    var typeTelephony : String?
    var arrProductosIncluidos: [ArrProductosIncluidos] = []
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    public func mapping(map: Map) {
    
        id <- map["Id"]
        name <- map["Nombre"]
        maximumAdd <- map["MaximoAgregar"]
        nameCommercial <- map["Nombrecomercial"]
        nameEditable <- map["NombreEditable"]
        planId <- map["PlanId"]
        price <- map["Precio"]
        isActivated <- map["SeActiva"]
        isInvoice <- map["SeFactura"]
        serviceId <- map["ServicioId"]
        srv_Mode <- map["SRV_Mode"]
        typeService <- map["TipoServicio"]
        typeTelephony <- map["TipoTelefonia"]
        arrProductosIncluidos <- map["ArrProductosIncluidos"]
        
        
        
}
}
