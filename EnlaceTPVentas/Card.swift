//
//  Card.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 25/09/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import Foundation
import ObjectMapper

class Card: NSObject,Mappable{
    
    var image : String?
    
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        
        image		<- map[""]
    }
}
