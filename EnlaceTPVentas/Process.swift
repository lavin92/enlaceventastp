//
//  Process.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 01/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class Process: NSObject, Mappable {
    
    var userID : String?
    var password : String?
    var ip : String?
    
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    public func mapping(map: Map) {

        userID <- map["UserID"]
        password <- map["Password"]
        ip <- map["Ip"]
}
}
