//
//  Opportunity.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 29/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class Opportunity: NSObject, Mappable {
    
    var nameOpportunity : String = ""
    var nameQuotation : String = ""
    var typePerson : String = ""
    var nameCompany : String = ""
    var reasonSocial : String = ""
    var rfc : String = ""
    var typeCoin : String = ""
    var name : String = ""
    var paternalLastName : String = ""
    var mothersLastName : String = ""
    var phonePrincipal : String = ""
    var email : String = ""
    var potentialAmount : String = ""
    var originProspectus : String = ""
    var street : String = ""
    var numberExterno : String = ""
    var numberInterno : String = ""
    var colony : String = ""
    var delegationMunicipality : String = ""
    var city : String = ""
    var state : String = ""
    var postalCode: String = ""
    var latitude : String = ""
    var longitude : String = ""
    var type : String = ""
    var subType : String = ""
    var plazo : String = ""
    var billingSegment : String = ""
    var stageName : String = ""
    var closeData : String = ""
    var sameDirection : String = ""
    
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    
    public func mapping(map: Map) {
        
        nameOpportunity <- map ["NombreOportunidad"]
        nameQuotation <- map ["NombreCotizacion"]
        typePerson <- map ["TipoPersona"]
        nameCompany <- map ["NombreCompania"]
        reasonSocial <- map ["RazonSocial"]
        rfc <- map ["RFC"]
        typeCoin <- map ["TipoMoneda"]
        name <- map ["Nombre"]
        paternalLastName <- map ["ApellidoPaterno"]
        mothersLastName <- map ["ApellidoMaterno"]
        phonePrincipal <- map ["TelefonoPrincipal"]
        email <- map ["CorreoElectronico"]
        potentialAmount <- map ["MontoPotencial"]
        originProspectus <- map ["OrigenProspecto"]
        street <- map ["Calle"]
        numberExterno <- map ["NumeroExterior"]
        numberInterno <- map ["NumeroInterior"]
        colony <- map ["Colonia"]
        delegationMunicipality <- map ["DelegacionMunicipio"]
        city <- map ["Ciudad"]
        state <- map ["Estado"]
        postalCode <- map ["CodigoPostal"]
        latitude <- map ["Latitude"]
        longitude <- map ["Longitude"]
        type <- map ["Tipo"]
        subType <- map ["SubTipo"]
        plazo <- map ["Plazo"]
        billingSegment <- map ["SegmentoFacturacion"]
        stageName <- map ["StageName"]
        closeData <- map ["CloseData"]
        sameDirection <- map ["MismaDireccion"]
        
        
        
        
        
        
    }
    
}

