//
//  ResultStatus.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 31/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

public class ResultStatus: NSObject, Mappable {

    var installedApproved : String?
    var installedPending : String?
    var installedRejected : String?
    var Inprocess : String?
    var approved : String?
    var pending : String?
    var rejected : String?
    
    required convenience public init?(map: Map) {
        self.init()
        
        }
    
    public func mapping(map: Map) {
        
    installedApproved <- map["InstaladasAprovadas"]
    installedPending <- map["InstaladasPendientes"]
    installedRejected <- map["InstaladasRechazadas"]
    Inprocess <- map["EnProceso"]
    approved <- map["Aprobadas"]
    pending <- map["Pendientes"]
    rejected <- map["Rechazadas"]
        
        
        
        
    }
    
}
