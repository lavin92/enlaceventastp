//
//  CreateOpportunityResponse.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 30/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class CreateOpportunityResponse:BaseResponse {
    
    var result : Result?
    var accountId : String = ""
    var opportunityId : String = ""
    var numberOpportunity : String = ""
    var quotationId : String = ""
    var contactId : String = ""
    var sites : [Sitios] = []
    
    
    
    
    public required convenience init?(map: Map){
        self.init()
    }
    
    
    public override func mapping (map: Map) {
       
        result <- map["Result"]
        accountId <- map["CuentaId"]
        opportunityId <- map["OportunidadId"]
        numberOpportunity <- map["NumeroOportunidad"]
        quotationId <- map["CotizacionId"]
        contactId <- map ["ConcactoId"]
        sites <- map ["Sitios"]
        
    }
}

