//
//  DocumentBean.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 16/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class DocumentBean: NSObject, Mappable {
    
    var name : String?
    var idOportunity : String?
    var brmAccount : String?
    var type : String?
    var body : String?
    var contentType : String?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    public func mapping(map: Map) {
        
        
        name <- map["Nombre"]
        idOportunity <- map["OportunidadId"]
        brmAccount <- map["CuentaBRM"]
        type <- map["Tipo"]
        body <- map["Body"]
        contentType <- map["ContentType"]
        
        
        }

}
