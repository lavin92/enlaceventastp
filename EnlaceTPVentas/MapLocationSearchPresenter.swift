//
//  MapLocationSearchPresenter.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 13/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//


import UIKit
import CoreLocation
import SwiftBaseLibrary
var mGeocodePresenter : GeocoderPresenter!

protocol MapLocationSearchDelegate : NSObjectProtocol {
    func onErrorCoverageValidate()
    func onSuccessCoverage(mCoberturaResponse : CoberturaResponse)
}

class MapLocationSearchPresenter: BaseVentasPresenter, CLLocationManagerDelegate{
    
    var mMapLocationSearchDelegate : MapLocationSearchDelegate!
    var mPosition : CLLocationCoordinate2D!
    
    init(viewController: BaseViewController, mapLocationSearchDelegate : MapLocationSearchDelegate) {
        super.init(viewController: viewController)
        self.mMapLocationSearchDelegate = mapLocationSearchDelegate
    }
    
    override func viewDidLoad() {
        requestLocation()
    }
    
    func requestLocation(){
        let locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    func loadStree (position : CLLocationCoordinate2D!){
        mGeocodePresenter.getAddressForLatLng(latitude: "\(position.latitude)", longitude: "\(position.longitude)")
        
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
    }
    
    func validateCoverage(position : CLLocationCoordinate2D!){
        mPosition = position
        let requestModel : ValidateCoverageRequest = ValidateCoverageRequest(latitude: "\(position.latitude)", longitude: "\(position.longitude)")
        
       
        RetrofitManager<CoberturaResponse>.init(requestUrl: ApiDefinition.WS_COVERAGE_VALIDATE, delegate: self).request(requestModel: requestModel)
    }
    
    func successLoadCoverageValidate(requestUrl : String, validateCoverageResponse : CoberturaResponse){
        if validateCoverageResponse.results?.rCode == "0" {
            mMapLocationSearchDelegate.onSuccessCoverage(mCoberturaResponse: validateCoverageResponse)
            let feasibility = validateCoverageResponse.feasibility
            if feasibility != nil {
                if (feasibility?.feasibility == ("1") || feasibility?.feasibility == ("2") || feasibility?.feasibility == ("6") || feasibility?.feasibility == ("7")  ){
                    
                    mMapLocationSearchDelegate.onSuccessCoverage(mCoberturaResponse: validateCoverageResponse)
                }else {
                    mMapLocationSearchDelegate.onErrorCoverageValidate()
                }
            }
        
            } else{
            mMapLocationSearchDelegate.onErrorCoverageValidate()
        }
    }
    
    override func onSuccessLoadResponse(requestUrl : String, response : BaseResponse){
        AlertDialog.hideOverlay()
        if (requestUrl == ApiDefinition.WS_COVERAGE_VALIDATE){
            successLoadCoverageValidate(requestUrl: requestUrl, validateCoverageResponse: response as! CoberturaResponse)
            
        }
    }
    
    override func onErrorLoadResponse(requestUrl : String, messageError : String){
        AlertDialog.hideOverlay()
        if (requestUrl == ApiDefinition.WS_COVERAGE_VALIDATE && messageError != ""){
            mMapLocationSearchDelegate.onErrorCoverageValidate()
        } else {
            AlertDialog.show( title: "Error", body: StringDialogs.dialog_error_intern, view: mViewController)
        }
    }
    
    
}
