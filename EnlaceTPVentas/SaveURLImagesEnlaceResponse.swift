//
//  SaveURLImagesEnlaceResponse.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 01/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class SaveURLImagesEnlaceResponse: NSObject, Mappable {
    
    var resultId : String?
    var result : String?
    var dscription : String?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {

        resultId <- map["ResultID"]
        result <- map["Result"]
        dscription <- map["Description"]
}
}
