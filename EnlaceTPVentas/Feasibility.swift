//
//  Feasibility.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 24/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

public class Feasibility: NSObject, Mappable {
    
    var feasible : String?
    var feasibility : String?
    var address : String?
    var percentage : String?
    var criterios : String?
    var comments : String?
    var idRegion : String?
    var region : String?
    var city : String?
    var distric : String?
    var zona : String?
    var name_Cluster : String?
    var coverage : String?
    
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    override init() {
        
    }
    
    
    init(region : String, distric : String, zona : String, name_Cluster : String) {
        self.region = region
        self.distric = distric
        self.zona = zona
        self.name_Cluster = name_Cluster
        
    }
    
    public func mapping(map: Map) {
        
        feasible <- map["factible"]
        feasibility <- map["factibilidad"]
        address <- map["direccion"]
        percentage <- map["porcentaje"]
        criterios <- map["criterios"]
        comments <- map["comentarios"]
        idRegion <- map["idRegion"]
        region <- map["Region"]
        city <- map["Ciudad"]
        distric <- map["distrito"]
        zona <- map["zona"]
        name_Cluster <- map["nombre_cluster"]
        coverage <- map["cobertura"]
        
        
    }
    
}

