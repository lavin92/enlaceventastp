//
//  ValidateCoverageRequest.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 19/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import ObjectMapper

class ValidateCoverageRequest: BaseVentasRequest {
    
    var validateCoverage : ValidateCoverage?
    var clientType : String?
    
    init(latitude : String, longitude : String) {
        super.init()
        self.clientType = "EMPRESARIAL";
        self.validateCoverage = ValidateCoverage()
        self.validateCoverage?.mData = Datam(vIdSite : "", vNameSite : "", vLatitude : latitude, vLongitude : longitude, vBandwidth : "", vTypeClient : self.clientType!)
    }
    
    public required init?(map: Map) {
        super.init()
    }
    
    internal override func mapping(map : Map){
        super.mapping(map: map)
        validateCoverage <- map["ValidateCoverage"]
    }
    
}

