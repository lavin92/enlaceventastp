//
//  LaunchPromotionsViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 26/09/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class LaunchPromotionsViewController: BaseViewController {
    
    
    @IBOutlet weak var launchPromotionsTableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.title =  "Lanzamientos"
    }
   
    
    
    
    var arrayItems = [Plans]()
    var plansDataSources: PlansDataSources!
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let obj: Plans = Plans()
        obj.tittle = "Superinternet Plus"
        obj.tittle2 = "500 Mbps descarga"
        obj.tittle3 = "20 Mbps carga"
        obj.image = "photo_01"
       arrayItems.append(obj)
        
        let obj1: Plans = Plans()
        obj1.tittle = "Seguridad perimetral"
        obj1.tittle2 = "Información segura"
        obj1.tittle3 = "20 Mbps carga. Incluye Wifi extender"
        obj1.image = "photo_02"
        arrayItems.append(obj1)
        
        let obj2: Plans = Plans()
        obj2.tittle = "Superinternet Plus"
        obj2.tittle2 = "500 Mbps descarga"
        obj2.tittle3 = "20 Mbps carga"
        obj2.image = "photo_01"
        arrayItems.append(obj2)
    
        
        
        
        let nib = UINib(nibName: "CellDetailTableViewCell", bundle: nil)
        launchPromotionsTableView.register(nib, forCellReuseIdentifier: "CellDetailTableViewCell")
        plansDataSources = PlansDataSources(tableView: self.launchPromotionsTableView)
        plansDataSources?.update(coleccionInfo: arrayItems)
        self.launchPromotionsTableView.reloadData()
    
    }

    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}

