//
//  Location.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 12/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

public class Location : NSObject, Mappable{
    
    var lat: Double?
    var lng: Double?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        lat        <- map["lat"]
        lng        <- map["lng"]
    }
}
