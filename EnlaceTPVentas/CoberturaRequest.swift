//
//  CoberturaRequest.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 31/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper


class CoberturaRequest: BaseVentasRequest{
    
    
    var validatecoverage : ValidateCoverage?
    
    
    override init() {
        super .init()
    }
    
    public required convenience init?(map: Map){
        self.init()
        
    }
    
    internal override func mapping (map: Map){
        
        super.mapping(map: map)
        validatecoverage <- map["ValidateCoverage"]
        
    }
    
    
    
}

