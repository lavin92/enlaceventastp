//
//  ServiceViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 27/09/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit

class ServiceViewController: UIViewController {
    
  
    @IBOutlet weak var serviceTableView: UITableView!
    
    
    
    var arrayItems = [List]()
    var listDataSource : ListDataSources!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let obj: List = List()
        obj.tittle = "1000 llamadas nacionales"
        //obj.image = "photo_01"
        arrayItems.append(obj)
        
        let obj1: List = List()
        obj1.tittle = "500 min LDI (Estados Unidos, Canada)"
        arrayItems.append(obj1)
        
        let obj2: List = List()
        obj2.tittle = "1000 min LDI (Estados Unidos, Canada)"
        arrayItems.append(obj2)
        
        let obj3: List = List()
        obj3.tittle = "500 min a celular (044/045)"
        arrayItems.append(obj3)
        
        let obj4: List = List()
        obj4.tittle = "1000 min a celular (044/045)"
        arrayItems.append(obj4)
        
        let obj5: List = List ()
        obj5.tittle = "IP Dinámica"
        arrayItems.append(obj5)
        
        let obj6: List = List ()
        obj6.tittle = "IP Fija"
        arrayItems.append(obj6)
        
        let obj7: List = List ()
        obj7.tittle = "Addon 50/5 Mbps"
        arrayItems.append(obj7)
        
        let obj8: List = List ()
        obj8.tittle = "Addon 100/10 Mbps"
        arrayItems.append(obj8)
        
        
        let nib = UINib(nibName: "CellAdditionalSettingsTableViewCell", bundle: nil)
        serviceTableView.register(nib, forCellReuseIdentifier: "CellAdditionalSettingsTableViewCell")
        listDataSource = ListDataSources(tableView: self.serviceTableView)
        listDataSource?.update(coleccionInfo: arrayItems)
        self.serviceTableView.reloadData()
        
  
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}

