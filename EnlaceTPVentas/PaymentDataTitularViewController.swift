//
//  PaymentDataTitularViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 05/12/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import TextFieldEffects

class PaymentDataTitularViewController: UIViewController, UITextFieldDelegate {

    let mValidator = UtilsTp()
    @IBOutlet weak var namesTextField: HoshiTextField!
    @IBOutlet weak var paternalLastNameTextField: HoshiTextField!
    @IBOutlet weak var maternalLastNameTextField: HoshiTextField!
    @IBOutlet weak var birthDateIconTextField: IconTextField!
    @IBOutlet weak var rfcTextField: HoshiTextField!
    @IBOutlet weak var phoneTextField: HoshiTextField!
    @IBOutlet weak var cityStateTextField: HoshiTextField!
    
    
    
   
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTextField{
            return mValidator.numbersAndLength(textField, shouldChangeCharactersIn: range, replacementString: string, maxChar: 10)
        }
        return true
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      birthDateIconTextField.setDatePicker()
        
      phoneTextField.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

    @IBAction func continueButton(_ sender: Any) {
        
        let textField1 = mValidator.textFieldDidChange(textField: namesTextField!)
        let textField2 = mValidator.textFieldDidChange(textField: paternalLastNameTextField!)
        let textField3 = mValidator.textFieldDidChange(textField: maternalLastNameTextField!)
        let textField4 = mValidator.textFieldDidChange(textField: rfcTextField!)
        let textField5 = mValidator.textFieldDidChange(textField: phoneTextField!)
        let textField6 = mValidator.textFieldDidChange(textField: cityStateTextField!)
        
        if ((textField1)&&(textField2)&&(textField3)&&(textField4)&&(textField5)&&(textField6)){
            
        }
        else{
            AlertDialog.show(title: "Error", body: "Favor de Llenar Todos los Campos", view: self)
        }
    }
    

}

