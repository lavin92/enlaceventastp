//
//  CotServicioProducto.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 19/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit

class CotServicioProducto: NSObject {
   
    var precioUnitarioBase : Double = 0
    var precioUnitarioProntoPago : Double = 0
    var precioUnitario : Double = 0
    var cantidad : Int = 0
    var descuento : Double = 0
    var impuesto1 : Double = 0
    var impuesto2 : Double = 0
    var tipoProducto : Int = 0
    var escargoUnico : Bool = false
    var esProntoPago : Bool = false
    var esProductoAdicional : Bool = false

    
    
    init(producto : AdicionalesBean) {

        precioUnitario = producto.getPrecioBase()
        precioUnitarioBase = producto.getPrecioBase()
        precioUnitarioProntoPago = producto.getPrecioProntoPago()
        cantidad = Int(producto.getCantidad())
        impuesto1 = producto.getIVA()
        impuesto2 = producto.getIEPS()
        
        
        
    }

}

