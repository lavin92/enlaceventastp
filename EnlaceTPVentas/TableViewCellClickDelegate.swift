//
//  TableViewCellClickDelegate.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 13/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//
import UIKit

public protocol TableViewCellClickDelegate: NSObjectProtocol {
    func onTableViewCellClick(item: NSObject, cell : UITableViewCell)
}

