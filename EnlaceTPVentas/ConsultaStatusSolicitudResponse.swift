//
//  ConsultaStatusSolicitudResponse.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 31/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary


class ConsultaStatusSolicitudResponse: BaseResponse  {

    var result : Result?
    var resultStatus : ResultStatus?
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public override func mapping(map: Map) {
    
    result <- map["Result"]
    resultStatus <- map["ResultStatus"]
        
        
    }
    
}
