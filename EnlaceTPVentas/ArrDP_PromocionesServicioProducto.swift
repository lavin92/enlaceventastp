//
//  ArrDP_PromocionesServicioProducto.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 13/03/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class ArrDP_PromocionesServicioProducto: NSObject, Mappable {
    
    var id : String?
    var name: String?
    var addService : String?
    var isChargeOnly : String?
    var status : String?
    var ieps : String?
    var iva : String?
    var startMonth : String?
    var amount__c : String?
    var planService : String?
    var productCharacteristic : String?
    var promotionPlan : String?
    var serviceProduct : String?
    var validityMonth : String?
    var idProductC : String?
    var nameProductC : String?
    var productId : String?
    var city : String?
    var hasDynamicIp : String?
    var hasFijaIp : String?
    var hasSTBAditional : String?
    var isCCTV : String?
    var quantity : String?
    var statusProductC : String?
    var isSoonPayment : String?
    var dp_ServicioProducto : DP_ServicioProducto?

    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
      
        id <- map["Id"]
        name <- map["Nombre"]
        addService <- map["AgregarServicio"]
        isChargeOnly <- map["EsCargoUnico"]
        status <- map["Estatus"]
        ieps <- map["IEPS"]
        iva <- map["IVA"]
        startMonth <- map["MesInicio"]
        amount__c <- map["Monto__c"]
        planService <- map["PlanServicio"]
        productCharacteristic <- map["ProductoCaracteristica"]
        promotionPlan <- map["PromocionPlan"]
        serviceProduct <- map["ServicioProducto"]
        validityMonth <- map["VigenciaMes"]
        idProductC <- map["IdProductoC"]
        nameProductC <- map["NameProductoC"]
        productId <- map["ProductoId"]
        city <- map["Ciudad"]
        hasDynamicIp <- map["TieneIPDinamica"]
        hasFijaIp <- map["TieneIPFija"]
        hasSTBAditional <- map["TieneSTBAdicional"]
        isCCTV <- map["EsCCTV"]
        quantity <- map["Cantidad"]
        statusProductC <- map["EstatusProductoC"]
        isSoonPayment <- map["EsProntoPago"]
        dp_ServicioProducto <- map["DP_ServicioProducto"]
    }
    
}
