//
//  Results.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 24/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class Results: NSObject, Mappable {

    var rIdResult : String?
    var rCode : String?
    var rDescription : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        
        rIdResult <- map["IdResult"]
        rCode <- map["Code"]
        rDescription <- map["Description"]
    }
}
    

