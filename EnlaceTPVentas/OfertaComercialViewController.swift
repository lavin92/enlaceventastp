//
//  OfertaComercialViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 08/05/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class OfertaComercialViewController: UIViewController {

    @IBOutlet weak var selectCityIconTextField: IconTextField!
    
    var mSelectCity: [String] = ["AGUASCALIENTES","CANCUN","CELAYA","CHIHUAHUA","CUERNAVACA","GUADALAJARA","CIUDAD JUAREZ","LEON","MERIDA","CIUDAD DE MÉXICO","MONTERREY","MORELIA","PACHUCA","PUEBLA","QUERETARO","SAN LUIS POTOSI","TIJUANA","TOLUCA","XALAPA","VERACRUZ","HERMOSILLO","TUXTLA GUTIÉRREZ"]
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.title =  "Oferta Comercial"
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
   selectCityIconTextField.setTextPicker(elements: mSelectCity)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
