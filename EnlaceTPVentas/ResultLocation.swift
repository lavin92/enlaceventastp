//
//  ResultLocation.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 12/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//
import UIKit
import ObjectMapper

public class ResultLocation : NSObject, Mappable{
    
    var geometry: Geometry?
    var formattedAddress : [AddressComponent] = []
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        geometry        <- map["geometry"]
        formattedAddress <- map["address_components"]
    }
}
