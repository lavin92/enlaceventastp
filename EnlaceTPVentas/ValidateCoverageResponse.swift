//
//  ValidateCoverageResponse.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 24/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class ValidateCoverageResponse: BaseResponse {
    var distanceFO : String?
    var olt : String?
    var frame: String?
    var slot: String?
    var port: String?
    var channeled : String?
    var poster : String?
    var rb1 : String?
    var distaceRb1: String?
    var cityEnlace : String?
    var typeFiber : String?
    var mediumAccess : String?
    var categoryService : String?
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    
    public override func mapping(map: Map) {
        
        distanceFO <- map["distanciaFO"]
        olt <- map["olt"]
        frame <- map ["frame"]
        slot <- map ["slot"]
        port <- map["puerto"]
        channeled <- map["canalizado"]
        poster <- map["posteria"]
        rb1 <- map["rb1"]
        distaceRb1 <- map["distanciaRb1"]
        cityEnlace <- map["ciudadesEnlace"]
        typeFiber <- map["tipoDeFibra"]
        mediumAccess <- map["medioDeAcceso"]
        categoryService <- map["CategoryService"]
        
        
    }
    
}

    

