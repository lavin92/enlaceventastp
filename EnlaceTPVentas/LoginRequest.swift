//
//  LoginRequest.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 13/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class LoginRequest: BaseVentasRequest {

    
    var nEmployee : String = ""
    var password : String = ""
    var id : String = ""
    
    override init() {
        super.init()
    }
 
    public required convenience init?(map: Map) {
        self.init()
    }
    
    internal override func mapping (map: Map) {
        super.mapping(map: map)
        nEmployee <- map["NoEmpleado"]
        password <- map["Password"]
        id <- map["IdSistema"]
    }

}
