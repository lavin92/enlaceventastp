//
//  CreateOpportunityRequest.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 30/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper

class CreateOpportunityRequest: BaseVentasRequest {
    
    var prospectus : Prospectus?
    
    
    override init() {
        super .init()
    }
    
    public required convenience init?(map: Map) {
        self.init()
        
    }
    
    internal override func mapping(map: Map) {
        super.mapping(map: map)
        prospectus <- map ["Prospectus"]
    }
}

