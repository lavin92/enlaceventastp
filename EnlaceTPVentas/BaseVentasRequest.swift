//
//  BaseVentasRequest.swift
//  EnlaceTPVentas
//
//  Created by Jorge Hdez VIlla on 27/11/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftBaseLibrary

class BaseVentasRequest: BaseRequest {
    
    var login : Login! = Login()
    
    override init() {
        login.user = "574011"
        login.password = "SalesF0rc31557$"
        login.ip = "127.0.0.1"
    }
    
    public required init?(map: Map) {
        
    }
    
    internal override func mapping (map: Map) {
        login <- map["Login"]
    }
    
}
