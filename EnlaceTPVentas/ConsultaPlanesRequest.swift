//
//  ConsultaPlanesRequest.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 27/10/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import ObjectMapper


class ConsultaPlanesRequest: BaseVentasRequest {
    
    
    
    var typeRegistrationCotation : String = ""
    var typeMoney : String = ""
    var city : String = ""
    var servicePlanType : String = ""
    var typeCoverage : String = ""
    var subtypeOpportunity : String = ""
    
    
    override init() {
        super.init()
        
    }
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    
    internal override func mapping (map: Map) {
        
        super.mapping(map: map)
        typeRegistrationCotation <- map["tipoRegistroCotizacion"]
        typeMoney <- map["tipoMoneda"]
        city <- map["Ciudad"]
        servicePlanType <- map["TipoPlanServicio"]
        typeCoverage <- map["TipoCobertura"]
        subtypeOpportunity <- map["SubTipoOportunidad"]
        
    }
}

