//
//  PredictionAddressDelegate.swift
//  EnlaceTPVentas
//
//  Created by Charls Salazar on 12/02/18.
//  Copyright © 2018 antonio lavin. All rights reserved.
//

import UIKit


public protocol PredictionAddressDelegate: NSObjectProtocol {
    
   
    func onSuccessPrediction(collectionAddress: [PredictionAddress])
    
    func onSuccessGeometry(geoLatLon: Geometry, formatAddress: [AddressComponent])
        

    
}


