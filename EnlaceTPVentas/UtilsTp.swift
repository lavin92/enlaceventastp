//
//  UtilsTp.swift
//  ventastpredesign
//
//  Created by Arturo Ceron Epigmenio on 16/04/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import TextFieldEffects
import PKHUD

class UtilsTp: NSObject {
    
    static var overlay : UIView?
    static var viewController : UIViewController?
    ////valida textfile lleno
    func textFieldDidChange(textField: HoshiTextField!)-> Bool {
        
          var valid : Bool = true
        if ((textField?.text?.count) != 0) {
            textField.placeholderColor = UIColor.darkGray
            textField.borderInactiveColor = UIColor.darkGray
            valid = true
            return valid
        }
        else {
            textField.placeholderColor = UIColor.red
            textField.borderInactiveColor = UIColor.red
            valid = false
            return valid
        }
        
    }
    
    
    func numbersAndLength(_ textField: UITextField,shouldChangeCharactersIn range: NSRange,replacementString string: String,maxChar: Int)-> Bool{
        if isOnlyNumbers(textField, shouldChangeCharactersIn: range, replacementString: string, maxChar: maxChar){
            return length(textField, shouldChangeCharactersIn: range, replacementString: string, maxChar: maxChar)
        }
        else {
            return false
        }
    }
    func isOnlyNumbers(_ textField: UITextField,shouldChangeCharactersIn range: NSRange,replacementString string: String,maxChar: Int) -> Bool {
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted;
        let compSepByCharInSet = string.components(separatedBy: aSet);
        let numberFiltenetworking = compSepByCharInSet.joined(separator: "a");
        
        if numberFiltenetworking == "a"{
            return false
        }
        else {
            return true
        }
    }
    
    func length(_ textField: UITextField,shouldChangeCharactersIn range: NSRange,replacementString string: String,maxChar: Int) -> Bool {
        let maxLength = maxChar
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength

    }
    

    /// textfil valida que este lleno y el numero maximo y minimo de caracteres
    
    func textFieldMaxMin(textField: HoshiTextField!, maxChar: Int, minChar: Int)-> Bool {
        
        var valid : Bool = true
        if ((textField?.text?.count) != 0 ) &&  ((textField?.text?.count)! >= minChar ) && ((textField?.text?.count)! <= maxChar ){
            valid = true
            textField.placeholderColor = UIColor.darkGray
            textField.borderInactiveColor = UIColor.darkGray
            return valid
        }
        else {
            textField.placeholderColor = UIColor.red
            textField.borderInactiveColor = UIColor.red
            valid = false
            return valid
        }
    }
    
    
    func isValidEmail(textField: HoshiTextField!) -> Bool {
        var valid : Bool = true
        var string = textField.text!
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let test = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        valid = test.evaluate(with: string)
        
        if  valid {
            textField.placeholderColor = UIColor.darkGray
            textField.borderInactiveColor = UIColor.darkGray
            return valid
            
        }
        else {
            textField.placeholderColor = UIColor.red
            textField.borderInactiveColor = UIColor.red
            return valid
        }
       
    }
    
    func isValidRFC(textField: HoshiTextField!) -> Bool {
        var valid : Bool = true
        var string = textField.text!
         let rfcRegEx = "[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?"
        let test = NSPredicate(format:"SELF MATCHES %@", rfcRegEx)
        valid = test.evaluate(with: string)
        if  valid {
            textField.placeholderColor = UIColor.darkGray
            textField.borderInactiveColor = UIColor.darkGray
            return valid
            
        }
        else {
            textField.placeholderColor = UIColor.red
            textField.borderInactiveColor = UIColor.red
            return valid
        }
       
    }
    
}
