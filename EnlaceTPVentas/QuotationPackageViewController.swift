//
//  QuotationPackageViewController.swift
//  EnlaceTPVentas
//
//  Created by antonio lavin on 04/12/17.
//  Copyright © 2017 antonio lavin. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class QuotationPackageViewController: BaseViewController {

    @IBOutlet weak var packagePriceLabel: UILabel!
    @IBOutlet weak var paySoonLabel: UILabel!
    @IBOutlet weak var additionalServicesLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var totalPaySoonLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    self.navigationItem.title = "Cotización"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func toAcceptButton(_ sender: Any) {
        ViewControllerUtils.pushViewController(from: self, to: UploadDocumentsViewController.self)
    }
    
    

}
